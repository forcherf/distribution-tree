################################################################################
# Enables testing.
# References: - https://github.com/ALPSCore/ALPSCore

set(TEST_RUNNER "" CACHE STRING "Command to launch test") # mpirun for mpi tests

include(CMakeParseArguments)
include_directories(${PROJECT_SOURCE_DIR}/libs/googletest-1.8.0/include)
enable_testing()

# Adds a test written with Google Test.
#
# dca_add_gtest(name
#               [INCLUDE_DIRS dir1 [dir2 ...]]
#               [SOURCES src1 [src2 ...]]
#               [LIBS lib1 [lib2 ...]])
#
# Adds a test called 'name', the source is assumed to be 'name.cpp'.
function(add_gtest name)
  set(multiValueArgs INCLUDE SOURCES LIBS)
  cmake_parse_arguments(ADD_GTEST "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  add_executable(${name} ${name}.cpp ${ADD_GTEST_SOURCES})

  target_include_directories(${name} PUBLIC ${ADD_GTEST_INCLUDE})

  # Create a macro with the project source dir. We use this as the root path for reading files in
  # tests.
  target_compile_definitions(${name} PRIVATE SOURCE_DIR=\"${PROJECT_SOURCE_DIR}\")

  # Use gtest main.
  target_link_libraries(${name} ${ADD_GTEST_LIBS} gtest gtest_main)


  add_test(NAME ${name} COMMAND ${TEST_RUNNER} "$<TARGET_FILE:${name}>")
endfunction()
