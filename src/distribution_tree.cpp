#include "distribution_tree.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <tuple>
#include <stack>
#include <algorithm>
#include <numeric>

#include "split.hpp"
#include "util/container_to_stream.hpp"

template <FitMode fit_mode, SplitMode split_mode>
void DistributionTree<fit_mode, split_mode>::construct(Matrix<Real>& data, Real alpha_fit,
                                                       Real alpha_indep) {
  const int d = data.nrRows();
  const int n = data.nrCols();

  const Real volume = normalize(data);

  elements_.emplace_back();
  auto& root = elements_[0];

  root.x = std::move(data);
  root.sze = std::vector<Real>(d);
  for (int i = 0; i < d; ++i) {
    root.sze[i] = ub_[i] - lb_[i];
  }

  std::stack<int> todoes;  // Indices of Elements to process.
  todoes.push(0);

  while (!todoes.empty()) {
    const int node_idx = todoes.top();
    todoes.pop();
    auto* node = &elements_[node_idx];

    std::array<int, 2> tosplit{node->to_split, -1};
    FitParameter<fit_mode> pars;

    if (node->x.nrCols() == 0) {
      node->to_split = tosplit[0] = -1;
      if constexpr (fit_mode == LINEAR)
        pars = std::vector(d, std::array<Real, 2>{0., 0.});
    }
    else if (node->to_split == -1) {
      std::tie(tosplit, pars) = dimstosplit<fit_mode>(node->x, node->sze, alpha_fit, alpha_indep);
    }

    if (tosplit[0] != -1) {
      node->to_split = tosplit[0];

      auto [sz1, x1, sz2, x2, split_point] =
          split(node->to_split, node->x, node->sze[node->to_split], split_mode);
      node->split_point = split_point;

      elements_.emplace_back();
      elements_.emplace_back();

      node = &elements_[node_idx];  // In case of reallocation of the vector of elements.
      node->child1 = elements_.size() - 2;
      auto* child1 = &elements_[node->child1];
      node->child2 = elements_.size() - 1;
      auto* child2 = &elements_[node->child2];

      child1->x = std::move(x1);
      child2->x = std::move(x2);

      child1->sze = node->sze;
      child1->sze[tosplit[0]] = sz1;

      child2->sze = node->sze;
      child2->sze[tosplit[0]] = sz2;

      if (tosplit[1] != -1) {
        child1->to_split = tosplit[1];
        child2->to_split = tosplit[1];
      }

      todoes.push(node->child2);
      todoes.push(node->child1);
    }
    else {  // Store fit parameters.
      const Real element_volume =
          std::accumulate(node->sze.begin(), node->sze.end(), 1., std::multiplies<Real>());

      if constexpr (fit_mode == UNIFORM) {
        node->fit_parameter = node->x.nrCols() / (element_volume * n);
      }
      else {  // Linear
        node->fit_parameter = std::move(pars);
        Real density = node->x.nrCols() / (element_volume * n);
        for (auto& elem : node->fit_parameter[0])
          elem *= density;
      }
    }

    // TODO: consider if clearing is necessary.
    node->sze.clear();
    node->x.clear();
  }
}

// Note: only uniform mode is implemented.
// TODO: (maybe) pass matrix of data return vector of estimates.
template <FitMode fit_mode, SplitMode split_mode>
Real DistributionTree<fit_mode, split_mode>::query(Real* x) const {
  const int d = lb_.size();
  assert(d == lb_.size() && d == ub_.size());

  // Normalize input x.
  for (int i = 0; i < d; ++i) {
    if (x[i] < lb_[i] || x[i] > ub_[i])
      return 0;

    x[i] = (x[i] - lb_[i]) / (ub_[i] - lb_[i]);
    assert(0 <= x[i] || 1 >= x[i]);
  }

  const Element* node = &elements_[0];  // Root.
  // Find element
  while (node->child1 != -1) {
    const Real cut = node->split_point;
    const int dim = node->to_split;

    if (x[dim] <= cut) {  // First child.
      x[dim] /= cut;
      node = &elements_[node->child1];
    }
    else {  // Second child.
      x[dim] = (x[dim] - cut) / (1. - cut);
      node = &elements_[node->child2];
    }
  }

  if constexpr (fit_mode == UNIFORM) {
    return node->fit_parameter;
  }
  else {
    Real prob = 1.;
    const auto& p = node->fit_parameter;
    for (int i = 0; i < d; ++i)
      prob *= p[i][0] + p[i][1] * x[i];
    return prob;
  }
}

template <FitMode fit_mode, SplitMode split_mode>
Real DistributionTree<fit_mode, split_mode>::normalize(Matrix<Real>& x) {
  lb_.resize(x.nrRows());
  ub_.resize(x.nrRows());
  Real volume = 1.;

  for (int i = 0; i < x.nrRows(); ++i) {
    lb_[i] = *std::min_element(x.row(i), x.row(i) + x.nrCols());
    ub_[i] = *std::max_element(x.row(i), x.row(i) + x.nrCols());
    volume *= ub_[i] - lb_[i];

    for (int j = 0; j < x.nrCols(); ++j) {
      x(i, j) = (x(i, j) - lb_[i]) / (ub_[i] - lb_[i]);
    }
  }

  return volume;
}

template <FitMode fit_mode, SplitMode split_mode>
void DistributionTree<fit_mode, split_mode>::write(const std::string& filename) const {
  std::ofstream out(filename);

  out << "lb ";
  for (auto x : lb_)
    out << x << " ";
  out << std::endl << "ub ";
  for (auto x : ub_)
    out << x << " ";

  out << "\n\n"
      << "# id child1 child2 to_split split_point [fit_parameter]\n"
      << "\n";

  int id = 0;
  for (auto& elem : elements_) {
    out << id << "\t" << elem.child1 << "\t" << elem.child2 << "\t" << elem.to_split << "\t"
        << elem.split_point;
    if (elem.to_split == -1)
      out << "\t" << elem.fit_parameter;

    out << "\n";
    ++id;
  }
}

template <FitMode fit_mode, SplitMode split_mode>
void DistributionTree<fit_mode, split_mode>::read(const std::string& filename) {
  std::ifstream inp(filename);
  std::string tmp, line_s;

  for (auto vec : std::array<std::vector<Real>*, 2>{&lb_, &ub_}) {
    std::getline(inp, line_s);
    std::stringstream line(line_s);
    line >> tmp;
    Real val;
    while (line >> val) {
      vec->push_back(val);
    }
  }

  std::getline(inp, line_s);
  std::getline(inp, line_s);

  int id;
  while (inp >> id) {
    elements_.emplace_back();
    auto& elem = elements_.back();

    inp >> elem.child1 >> elem.child2 >> elem.to_split >> elem.split_point;

    if (elem.to_split == -1) {
      if constexpr (fit_mode == LINEAR)
        elem.fit_parameter.resize(lb_.size());
      inp >> elem.fit_parameter;
    }
  }
}

template <FitMode fit_mode, SplitMode split_mode>
bool DistributionTree<fit_mode, split_mode>::compare(const DistributionTree<fit_mode, split_mode>& other,
                                                     Real tolerance) const {
  if(lb_.size() != other.lb_.size())
    return false;

  for (int i = 0; i < lb_.size(); ++i) {
    if (std::abs(lb_[i] - other.lb_[i]) > tolerance || std::abs(ub_[i] - other.ub_[i]) > tolerance)
      return false;
  }

  if (elements_.size() != other.elements_.size())
    return false;

  for (int i = 0; i < elements_.size(); ++i) {
    if (elements_[i].to_split != other.elements_[i].to_split)
      return false;
    if (elements_[i].child1 != other.elements_[i].child1)
      return false;
    if (std::abs(elements_[i].split_point - other.elements_[i].split_point) > tolerance)
      return false;
    if constexpr (fit_mode == UNIFORM) {
      if (std::abs(elements_[i].fit_parameter - other.elements_[i].fit_parameter) > tolerance)
        return false;
    }
  }

  return true;
}

template class DistributionTree<UNIFORM, MEDIAN>;
template class DistributionTree<LINEAR, MEDIAN>;
template class DistributionTree<UNIFORM, HALF>;
template class DistributionTree<LINEAR, HALF>;
