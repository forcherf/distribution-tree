#include "split.hpp"

#include <algorithm>
#include <array>

#include "statistical_tests/chi_squared_pvalue_test.hpp"
#include "statistical_tests/independence_test.hpp"
#include "typedefs.hpp"

// In: mode. 0 = uniform, 1 = linear.
// In: x. dim x n_data matrix
//
// return: dimension to slit. (int)
// // Not yet. return: list of fit parameters.
template <FitMode mode>
std::tuple<std::array<int, 2>, FitParameter<mode>> dimstosplit(const Matrix<Real>& x,
                                                               const std::vector<Real>& sze,
                                                               Real alpha_fit, Real alpha_indep) {
  const int dim = x.nrRows();
  const int n = x.nrCols();

  std::array<int, 2> to_split{-1, -1};
  FitParameter<mode> pars(dim);

  if (n == 0) {
    return std::make_tuple(to_split, std::move(pars));
  }

  std::vector<Real> marginal_pvals(dim);
  Real min_pval = 2;
  int worst_dim = -1;

  // Test goodness of fit in each dimension.
  Real pval;
  bool reject;
  for (int i = 0; i < dim; ++i) {
    if constexpr (mode == UNIFORM) {
      std::tie(reject, pval) = chiSquaredUniformTest(x.row(i), n, alpha_fit);
    }
    else { // Linear.
      std::tie(reject, pval, pars[i]) = chiSquaredLinearTest(x.row(i), n, alpha_fit);
    }
    marginal_pvals[i] = pval;
    // Note: the dimension size acts as a tiebreaker.
    if (reject && ((pval < min_pval) || (pval == min_pval && sze[i] > sze[worst_dim]))) {
      min_pval = pval;
      worst_dim = i;
    }
  }

  if (worst_dim != -1) {
    to_split[0] = worst_dim;
    return std::make_tuple(to_split, std::move(pars));
  }

  // Test data independence.
  auto [reject_m, pvals_m] = chi2IndepTest(x, alpha_indep);

  int corr_i(-1), corr_j(-1);
  min_pval = 1;
  Real to_split_size = 0;

  for (int i = 0; i < dim; ++i)
    for (int j = i + 1; j < dim; ++j) {
      // Note: the dimension size acts as a tiebreaker.
      const Real size = sze[i] * sze[j];
      if (reject_m(i, j) &&
          ((pvals_m(i, j) < min_pval) || (pvals_m(i, j) == min_pval && size > to_split_size))) {
        min_pval = pvals_m(i, j);
        corr_i = i;
        corr_j = j;
        to_split_size = size;
      }
    }

  if (corr_i != -1) {  // There are correlated dimensions.
    if (marginal_pvals[corr_i] < marginal_pvals[corr_j])
      to_split = std::array<int, 2>{corr_i, corr_j};
    else
      to_split = std::array<int, 2>{corr_j, corr_i};
  }

  return std::make_tuple(to_split, std::move(pars));
}

template std::tuple<std::array<int, 2>, FitParameter<UNIFORM>> dimstosplit<UNIFORM>(
    const Matrix<Real>&, const std::vector<Real>&, Real, Real);
template std::tuple<std::array<int, 2>, FitParameter<LINEAR>> dimstosplit<LINEAR>(
    const Matrix<Real>&, const std::vector<Real>&, Real, Real);

Real median(const Real* x, const int n) {
  std::vector<Real> sorted(n);
  std::copy_n(x, n, sorted.begin());
  std::sort(sorted.begin(), sorted.end());

  if (n % 2 == 1) {
    return sorted[n / 2];
  }
  else
    return (sorted[n / 2 - 1] + sorted[n / 2]) / 2.;
}

std::tuple<Real, Matrix<Real>, Real, Matrix<Real>, Real> split(int dim, const Matrix<Real>& x,
                                                               Real size, SplitMode mode) {
  const int n = x.nrCols();
  assert(n > 0);

  const Real split_pos = mode == HALF ? 0.5 : median(x.row(dim), x.nrCols());

  const Real size1 = size * split_pos;
  const Real size2 = size * (1. - split_pos);

  // TODO: optimize if median was already computed and x sorted.
  // TODO: (maybe) implement with a single scan.
  int count1 = 0;
  for (int j = 0; j < n; ++j) {
    if (x(dim, j) <= split_pos)
      count1++;
  }

  // Copy data into new subcuboids.
  Matrix<Real> x1(x.nrRows(), count1);
  Matrix<Real> x2(x.nrRows(), n - count1);
  int j1 = 0, j2 = 0;
  const Real norm1 = 1. / split_pos;
  const Real norm2 = 1. / (1. - split_pos);
  for (int j = 0; j < n; ++j) {
    if (x(dim, j) <= split_pos) {
      for (int i = 0; i < x.nrRows(); ++i)
        x1(i, j1) = x(i, j);
      // Scale into [0, 1].
      x1(dim, j1) *= norm1;
      ++j1;
    }
    else {
      for (int i = 0; i < x.nrRows(); ++i)
        x2(i, j2) = x(i, j);
      // Scale into [0, 1].
      x2(dim, j2) = (x2(dim, j2) - split_pos) * norm2;
      ++j2;
    }
  }

  return std::make_tuple(size1, std::move(x1), size2, std::move(x2), split_pos);
}
