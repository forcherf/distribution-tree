#include "util/generate_data.hpp"

#include <random>

Matrix<Real> generateData(int n, int d, std::size_t seed) {
  std::mt19937_64 rng(seed);
  std::normal_distribution<Real> normal(0., 1.);

  Matrix<Real> data(d, n);
  for (int i = 0; i < d; ++i)
    for (int j = 0; j < n; ++j) {
      data(i, j) = normal(rng);
    }

  Matrix<Real> cov_sqr(d, d);
  std::uniform_real_distribution<Real> uni(-1., 1.);
  for (int i = 0; i < d; ++i)
    for (int j = 0; j < d; ++j) {
      cov_sqr(i, j) = uni(rng);
    }
  cov_sqr = square(cov_sqr);

  data = cov_sqr * data;
  return data;
}

Matrix<Real> generateUniform(int n, int d, std::size_t seed) {
  std::mt19937_64 rng(seed);
  std::uniform_real_distribution<Real> distro(0., 1.);

  Matrix<Real> data(d, n);
  for (int i = 0; i < d; ++i)
    for (int j = 0; j < n; ++j) {
      data(i, j) = distro(rng);
    }

  return data;
}
