add_library(util generate_data.cpp counters.cpp)

find_package(BLAS REQUIRED)
target_link_libraries(util ${BLAS_LIBRARIES})
