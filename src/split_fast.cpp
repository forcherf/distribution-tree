#include "split.hpp"

#include <algorithm>
#include <array>

#include "statistical_tests/chi_squared_pvalue_test.hpp"
#include "statistical_tests/chi_squared_cdf.hpp"
#include "statistical_tests/independence_test.hpp"
#include "statistical_tests/histograms.hpp"
#include "typedefs.hpp"

namespace fast {

// In: mode. 0 = uniform, 1 = linear.
// In: x. dim x n_data matrix
//
// return: dimension to slit. (int)
// // Not yet. return: list of fit parameters.
template <>
std::array<int, 2> dimstosplit<LINEAR>(const MatrixView<Real> &x, Real alpha_fit, Real alpha_indep,
                                       Vector<Real> &slopes, Vector<Real> &sums,
                                       Vector<int> &ks, Vector<int> &co2d,
                                       Vector<Real> &x_sorted, Matrix<Real> &be,
                                       Vector<int> &co) {
  const int dim = x.nrRows();
  const int n = x.nrCols();

  assert(dim == slopes.size());

  std::array<int, 2> to_split{-1, -1};

  const int n_bins = nbins(n, alpha_fit);

  if (n_bins <= 1) {
    return to_split;
  }

  Real max_sum = 0;
  int worst_dim = -1;

  be.resize(dim, n_bins + 1);
  co.resize(n_bins);
  ks.resize(n_bins);

  int old_ns = -1;
  for (int j = 0; j < n_bins - 1; ++j) {  // loop over bins except last and first.
    int ns = round(static_cast<Real>(j + 1) * static_cast<Real>(n) /
                   n_bins);  // index of last sample in current bin
    ks[j] = ns;
    co[j] = ns - old_ns;
    old_ns = ns;
  }
  co[n_bins - 1] = n - 1 - old_ns;
  ks[n_bins - 1] = n;

  // Test goodness of fit in each dimension.
  for (int i = 0; i < dim; ++i) {
    // NOTE: missing uniform mode
    std::tie(sums[i], slopes[i]) =
        fast::chiSquaredLinearTest(x.row(i), n, n_bins, co.data(), be.row(i), x_sorted, ks);
    if (sums[i] > max_sum) {
      worst_dim = i;
      max_sum = sums[i];
    }
  }

  if (n_bins <= 2) {
    return to_split;
  }

  const Real pval_linear = pValue(sums[worst_dim], n_bins - 2);

  if (pval_linear < alpha_fit) {
    to_split[0] = worst_dim;
    return to_split;
  }

  // Test data independence.
  auto [max_sum2, corr_i, corr_j] = chi2IndepTest(x, n_bins, co, co2d, be);

  const Real pval2 = pValue(max_sum2, (n_bins - 1) * (n_bins - 1));

  if (pval2 < alpha_indep) {  // There are correlated dimensions.
    if (sums[corr_i] > sums[corr_j])
      to_split = std::array<int, 2>{corr_i, corr_j};
    else
      to_split = std::array<int, 2>{corr_j, corr_i};
  }

  return to_split;
}

// TODO: optimize.
Real median(const Real* x, const int n) {
  Vector<Real> sorted(n);
  std::copy_n(x, n, sorted.begin());
  std::sort(sorted.begin(), sorted.end());

  if (n % 2 == 1) {
    return sorted[n / 2];
  }
  else
    return (sorted[n / 2 - 1] + sorted[n / 2]) / 2.;
}

std::tuple<MatrixView<Real>, MatrixView<Real>, Real> split(int dim, MatrixView<Real>& x,
                                                           SplitMode mode) {
  const int n = x.nrCols();
  const int d = x.nrRows();
  assert(n > 0);

  const Real split_pos = mode == HALF ? 0.5 : median(x.row(dim), x.nrCols());

  // TODO: optimize if median was already computed and x sorted.
  // TODO: (maybe) implement with a single scan.
  int left = 0, right = n - 1;
  Real* const row = x.row(dim);

  // pivot in place
  // TODO check
  while (true) {
    while (row[left] <= split_pos && left < n) {
      ++left;
    }
    while (row[right] > split_pos && right >= 0) {
      --right;
    }

    if (left >= right)
      break;

    // Swap
    for (int i = 0; i < d; ++i)
      std::swap(x(i, left), x(i, right));
  }

  // Reference data into new subcuboids.
  MatrixView<Real> x1(x, left, 0);
  MatrixView<Real> x2(x, n - left, left);

  // Normalize
  const Real norm1 = 1. / split_pos;
  const Real norm2 = 1. / (1. - split_pos);
  for (int i = 0; i < left; ++i)
    row[i] *= norm1;
  for (int i = left; i < n; ++i)
    row[i] = (row[i] - split_pos) * norm2;

  return std::make_tuple(x1, x2, split_pos);
}

}  // namespace fast
