#include "statistical_tests/find_statistics.hpp"

#include <algorithm>
#include <cassert>

namespace fast {

void __findStatistics(Real* x, Real* end, const int* k, int nk, Real* x_orig) {
  if (end <= x or nk <= 0)
    return;

  const int half_k = nk / 2;
  assert(x <= x_orig + k[half_k]);
  assert(x_orig + k[half_k] <= end);
  std::nth_element(x, x_orig + k[half_k], end);

  __findStatistics(x, x_orig + k[half_k], k, half_k, x_orig);
  __findStatistics(x_orig + k[half_k] + 1, end, k + half_k + 1, nk - half_k - 1, x_orig);
}

void findStatistics(Real* x, int n, const int* k, int nk) {
  if (nk > 64)
    __findStatistics(x, x + n, k, nk, x);
  else
    std::sort(x, x + n);
}

}  // namespace fast
