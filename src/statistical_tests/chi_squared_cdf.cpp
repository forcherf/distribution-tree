#include "statistical_tests/chi_squared_cdf.hpp"

#include <cmath>
#include <stdexcept>

#ifdef DET_USE_GSL
#include <gsl/gsl_cdf.h>
#endif

constexpr Real tolerance = 1e-7;
constexpr Real tiny = 1e-290;

// Regularized upper incomplete gamma function, by continued fraction
// Based on the implementation of the function `_kf_gammaq` of `htslib 1.3`.
Real incUGamma(Real s, Real z) {
  int k;
  Real C, D, f;
  f = 1. + z - s;
  C = f;
  D = 0.;
  // Modified Lentz's algorithm for computing continued fraction.
  const int limit = 200;
  for (k = 1; k < limit; ++k) {
    Real a = k * (s - k), b = (k << 1) + 1 + z - s, d;
    D = b + a * D;
    if (D < tiny)
      D = tiny;
    C = b + a / C;
    if (C < tiny)
      C = tiny;
    D = 1. / D;
    d = C * D;
    f *= d;
    if (fabs(d - 1.) < tolerance)
      break;
  }
  if (k == limit)
    throw(std::logic_error("incUGamma failed to converge."));

  return exp(s * log(z) - z - std::lgamma(s) - log(f));
}

// Regularized lower incomplete gamma function, by series expansion.
// Based on the implementation of the function `_kf_gammap` of `htslib 1.3`.
Real incLGamma(Real s, Real z) {
  Real sum, x;
  int k;
  const int limit = 200;
  for (k = 1, sum = x = 1.; k < limit; ++k) {
    x *= z / (s + k);
    sum += x;
    if (x / sum < tolerance)
      break;
  }
  if (k == limit)
    throw(std::logic_error("incLGamma failed to converge."));

  return exp(s * log(z) - z - std::lgamma(s + 1.) + log(sum));
}

// Cumulative chi2 distribution
Real chiSquaredCdf(Real x, int dof) {
  if (x < 0 or dof <= 0)
    throw(std::logic_error("The cdf is defined only for positive arguments"));

#ifdef DET_USE_GSL
return gsl_cdf_chisq_P(x, dof);

#else
    if (x == 0)
    return 0;

  if (x < dof)
    return incLGamma(0.5 * dof, 0.5 * x);

  return 1 - incUGamma(0.5 * dof, 0.5 * x);
#endif  // DET_USE_GSL
}
