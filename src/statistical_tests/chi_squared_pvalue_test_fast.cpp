#include "statistical_tests/chi_squared_pvalue_test.hpp"

#include "statistical_tests/histograms.hpp"
#include "distribution_tree.hpp"
#include "util/counters.hpp"

#include <numeric>

#ifndef SQR
#define SQR(x) ((x) * (x))
#endif

namespace fast {

//  TODO: rename (LinearChiSqauredTestSum ?)
std::tuple<Real, Real>
chiSquaredLinearTest(const Real *x, const int n, const int k, int *co, Real *be,
                     Vector<Real> &x_s, Vector<int> &ks) {
  chi2testtable(x, n, k, be, x_s, ks);

  if (k <= 2)
    return std::make_tuple(0, 0);

  const auto [mu, var] = meanStdv(x, n);

  // Check for s  =0, mu = 1/2 ?
  Real s = 6 * (2 * mu - 1);
  Real cf = -1;
  if (n > 1) {
    // correction factor cf to minimize mean square error
    cf = (n * s * s) / (n * s * s + 4 * 36 * var);  // stats::var(x)
  }
  s = cf * s;
  // TODO: check s.

  // Limit s to have positive pdf.
  const Real limit = 2.;  // (SQR(be.back()) - SQR(be.front()));
  s = std::max(-limit, s);
  s = std::min(limit, s);

  // Note: assume data in [0, 1].
  const Real offset =
      (1 - 0.5 * s);  //* (SQR(be.back()) - SQR(be.front()))) / (be.back() - be.front());

  Real sum = 0.;
  for (int i = 0; i < k; ++i) {
    const Real ce = n * (offset * (be[i + 1] - be[i]) + 0.5 * s * (SQR(be[i + 1]) - SQR(be[i])));
    sum += SQR(co[i] - ce) / ce;
  }

  counters::flop += 13 * k;
  // double pval = 1. - chiSquaredCdf(sum, co.size() - 2);

  return std::make_tuple(sum, s);
}

}  // namespace fast
