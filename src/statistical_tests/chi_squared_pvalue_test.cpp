#include "statistical_tests/chi_squared_pvalue_test.hpp"

#include "statistical_tests/histograms.hpp"
#include "statistical_tests/chi_squared_cdf.hpp"

#include <numeric>

#ifndef SQR
#define SQR(x) ((x) * (x))
#endif

std::tuple<Bool, Real> chiSquaredUniformTest(const Real* x, int n, Real alpha) {
  auto [co, be] = chi2testtable(x, n, alpha);
  const int k = co.size();
  if (k <= 1)
    return std::make_tuple(0, 0);

  Real sum = 0.;
  const Real density = n /*/ (be.back() - be.front())*/;  // Assume data in [0,1]
  for (int i = 0; i < k; ++i) {
    const double ce = (be[i + 1] - be[i]) * density;
    sum += SQR(co[i] - ce) / ce;
  }

  double pval = 1. - chiSquaredCdf(sum, co.size() - 1);

  // QUESTION: do you need to return the test acceptance?
  return std::make_tuple(pval <= alpha, pval);
}

std::tuple<Bool, Real, std::array<Real, 2>> chiSquaredLinearTest(const Real* x, const int n,
                                                                 const Real alpha) {
  auto [co, be] = chi2testtable(x, n, alpha);
  const int k = co.size();

  if (k <= 2)
    return std::make_tuple(0, 0, std::array<Real, 2>{1, 0});

  const auto [mu, var] = meanStdv(x, n);

  // Check for s  =0, mu = 1/2 ?
  Real s = 6 * (2 * mu - 1);
  Real cf = -1;
  if (n > 1) {
    // correction factor cf to minimize mean square error
    cf = (n * s * s) / (n * s * s + 4 * 36 * var);  // stats::var(x)
  }
  s = cf * s;
  // TODO: check s.

  // Limit s to have positive pdf.
  const Real limit = 2.;  // (SQR(be.back()) - SQR(be.front()));
  s = std::max(-limit, s);
  s = std::min(limit, s);

  // Note: assume data in [0, 1].
  const Real offset =
      (1 - 0.5 * s);  //* (SQR(be.back()) - SQR(be.front()))) / (be.back() - be.front());

  Real sum = 0.;
  for (int i = 0; i < k; ++i) {
    const Real ce = n * (offset * (be[i + 1] - be[i]) + 0.5 * s * (SQR(be[i + 1]) - SQR(be[i])));
    sum += SQR(co[i] - ce) / ce;
  }

  double pval = 1. - chiSquaredCdf(sum, co.size() - 2);

  return std::make_tuple(pval <= alpha, pval, std::array<Real, 2>{offset, s});
}
