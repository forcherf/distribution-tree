#include "statistical_tests/independence_test.hpp"

#include <algorithm>
#include <cmath>
#include <tuple>
#include <vector>
#include <immintrin.h>

#include "matrix.hpp"
#include "statistical_tests/chi_squared_cdf.hpp"
#include "statistical_tests/histograms.hpp"
#include "statistical_tests/util.hpp"
#include "util/counters.hpp"

namespace fast {

// Returns max_sum, idx1, idx2.
std::tuple<Real, int, int> chi2IndepTest(const MatrixView<Real>& x, int n_bins,
                                         const Vector<int>& co_v, Vector<int>& co2d,
                                         const Matrix<Real>& be) {
  int corr_i(-1), corr_j(-1);
  Real max_sum = 0;
  if (n_bins <= 1)
    return std::make_tuple(max_sum, corr_i, corr_j);

  const int dim = x.nrRows();
  const int n = x.nrCols();
  const int n_padded = ((n + 7) / 8) * 8;
  const int n_bins_padded = ((n_bins + 3) / 4) * 4;

  static Vector<int> bin_id;
  bin_id.resize(dim * n_padded);

  auto bin_row = [&](int i) { return bin_id.data() + i * n_padded; };

  for (int i = 0; i < dim; ++i)
    findBins(x.row(i), be.row(i), n_bins, n, bin_row(i));

  co2d.resize(n_bins_padded * n_bins);
  const Real one_over_n = 1. / n;
  const int* const co = co_v.data();
  int* const co2 = co2d.data();

  // TODO: histograms can be reused.
  for (int d1 = 0; d1 < dim; ++d1) {
    for (int d2 = d1 + 1; d2 < dim; ++d2) {
      // contingency table with observed counts
      histcounts2(bin_row(d1), bin_row(d2), n_bins, n, co2, n_bins_padded);

      __m256d sums = _mm256_set1_pd(0.);

      for (int i = 0; i < n_bins; ++i) {
        auto c_i = _mm256_set1_pd(co[i] * one_over_n);
        int j;
        for (j = 0; j < n_bins - 3; j += 4) {
          __m128i co_int = _mm_load_si128((__m128i*)(co2 + i * n_bins_padded + j));
          __m128i ce_int = _mm_load_si128((__m128i*)(co + j));

          auto co = _mm256_cvtepi32_pd(co_int);  // count observed.
          auto ce = _mm256_cvtepi32_pd(ce_int);  // count expected.
          ce = _mm256_mul_pd(c_i, ce);

          auto diff = _mm256_sub_pd(ce, co);
          auto square = _mm256_mul_pd(diff, diff);
          auto contrib = _mm256_div_pd(square, ce);
          sums = _mm256_add_pd(sums, contrib);
        }
        for (; j < n_bins; ++j) {  // remainder
          const Real ce = c_i[0] * co[j];
          const Real co = co2d[i * n_bins_padded + j];
          sums[0] += (co - ce) * (co - ce) / ce;
        }
      }

      const Real sum = (sums[0] + sums[1]) + (sums[2] + sums[3]);
      if (sum > max_sum) {
        max_sum = sum;
        corr_i = d1;
        corr_j = d2;
      }
    }
  }


  counters::flop += 5 * n_bins * n_bins * dim * dim;

  return std::make_tuple(max_sum, corr_i, corr_j);
}

}  // namespace fast
