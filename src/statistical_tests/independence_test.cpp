#include "statistical_tests/independence_test.hpp"
#include "util/counters.hpp"

#include <algorithm>
#include <cmath>
#include <tuple>
#include <vector>

#include "matrix.hpp"
#include "statistical_tests/chi_squared_cdf.hpp"
#include "statistical_tests/histograms.hpp"
#include "statistical_tests/util.hpp"

std::tuple<Matrix<Bool>, Matrix<Real>> chi2IndepTest(const Matrix<Real>& x, Real alpha) {
  const int dim = x.nrRows();
  const int n = x.nrCols();
  Matrix<Real> pval(dim, dim, 0.);
  Matrix<Bool> res(dim, dim, 1);  // TODO: (maybe) only upper triangle is necessary.

  // TODO: histograms can be reused.
  for (int i = 0; i < dim; ++i) {
    auto [co1, be1] = chi2testtable(x.row(i), n, alpha);
    const auto n1 = co1.size();
    for (int j = i + 1; j < dim; ++j) {
      auto [co2, be2] = chi2testtable(x.row(j), n, alpha);
      const auto n2 = co2.size();
      // no need to test if there is just one bin
      if (n1 == 1 || n2 == 1) {
        res(i, j) = 0;  // accept.
        res(j, i) = 0;  // accept.
        continue;
      }

      // contingency table with observed counts
      auto co = histcounts2(x.row(i), x.row(j), be1, be2, n);

      // expected counts ce = c01 * co2^t / dim;
      Real sum = 0;  // \sum((co - ce)**2 / ce)
      for (int i = 0; i < n1; ++i)
        for (int j = 0; j < n2; ++j) {
          const Real ce = co1[i] * co2[j] / static_cast<Real>(n);  // count expected.
          const Real co_val = co[i * n1 + j];                      // count observed.
          sum += (ce - co_val) * (ce - co_val) / ce;
        }

      counters::flop += 5 * n1 * n2;
      // chi^2 independence test

      pval(i, j) = pval(j, i) =
          1 - chiSquaredCdf(sum, (n1 - 1) * (n2 - 1));  // Probability of independence.
      res(i, j) = res(j, i) = pval(i, j) < alpha;
    }
  }



  return std::make_tuple(std::move(res), std::move(pval));
}
