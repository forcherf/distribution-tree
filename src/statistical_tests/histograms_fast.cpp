#include <algorithm>
#include <cassert>
#include <cstring>
#include <numeric>
#include <vector>
#include <unordered_map>

#include "matrix.hpp"
#include "statistical_tests/util.hpp"
#include "statistical_tests/find_statistics.hpp"

#define SQR(x) ((x) * (x))

namespace fast {

// In: x: data
// In: be: bin edges.
// In k: number of bins
// Out bin_id: tag of the bin where the datapoint is.
void findBins(const Real* __restrict__ x, const Real* __restrict__ be, int k, int n,
              int* __restrict__ bin_id) {
  for (int i = 0; i < n; ++i) {
    const auto bin_ptr = std::lower_bound(be + 1, be + k + 1, x[i]);
    bin_id[i] = bin_ptr - be - 1;
  }
}

// In: ld: leading dimension of co2d
// Out: co2d
void histcounts2(const int* __restrict__ x1, const int* __restrict__ x2, int k, int n,
                 int* __restrict__ co2d, int ld) {
  std::memset(co2d, 0, ld * k * sizeof(int));

  for (int i = 0; i < n; ++i) {
    ++co2d[x1[i] * ld + x2[i]];
  }
}

int nbins(int n, Real alpha) {
  // Compute and cache c.
  static std::unordered_map<Real, Real> c_map;
  Real c;
  if (c_map.find(alpha) == c_map.end()) {
    c = std::sqrt(2) * erfinv(1. - 2 * alpha);
    c_map[alpha] = c;
  }
  else {
    c = c_map[alpha];
  }

  Real ks = n / 5.;
  Real kp = 4 * std::pow(2 * SQR((n - 1) / c), 1. / 5.);

  //  if constexpr (multivariate) {
  //    ks = sqrt(ks);
  //    kp = sqrt(kp);
  //  }
  return std::max(1, int(std::min(ks, kp)));
}

Real min_element(Real* start, Real* end) {
  Real min = *start;
  //  ++start;
  while (start != end) {
    if (*start < min)
      min = *start;
    ++start;
  }
  return min;
}

void chi2testtable(const Real* x, const int n, const int n_bins, Real* be, Vector<Real>& x_sorted,
                   Vector<int>& k) {
  // put data into bins
  // TODO: consider sorting and storing once.
  std::copy_n(x, n, x_sorted.begin());
  findStatistics(x_sorted.data(), n, k.data(), k.size());
  be[0] = 0;  // TODO: remove.
  for (int j = 0; j < n_bins - 1; ++j) {
    be[j + 1] = x_sorted[k[j]];
    be[j + 1] += min_element(x_sorted.data() + k[j] + 1, x_sorted.data() + k[j + 1]);
    be[j + 1] /= 2.;
  }
  be[n_bins] = 1;  // TODO: maybe remove.
}

}  // namespace fast
