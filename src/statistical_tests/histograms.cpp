#include "statistical_tests/histograms.hpp"

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>

#include "statistical_tests/util.hpp"

#define SQR(x) ((x) * (x))

std::vector<int> histcounts(const Real* x, const std::vector<Real>& be, int n) {
  const int k = be.size() - 1;
  std::vector<int> co(k, 0);
  // TODO optimize. (Use sorted?)
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < k; ++j)
      if (x[i] <= be[j + 1]) {
        ++co[j];
        break;
      }
  }
  // Check the total count
  assert(std::accumulate(co.begin(), co.end(), 0.) == n);
  return co;
}

std::vector<int> histcounts2(const Real* __restrict__ x1, const Real* __restrict__ x2,
                             const Vector& be1, const Vector& be2, int n) {
  const int k1 = be1.size() - 1;
  const int k2 = be2.size() - 1;
  std::vector<int> co(k1 * k2, 0);
  // TODO optimize. (Use sorted?)
  for (int i = 0; i < n;) {
    for (int b1id = 0; b1id < k1; ++b1id)
      if (x1[i] <= be1[b1id + 1]) {
        for (int b2id = 0; b2id < k2; ++b2id)
          if (x2[i] <= be2[b2id + 1]) {
            ++co[b1id * k1 + b2id];
            goto loop_end;
          }
      }
  loop_end:
    ++i;
  }

  assert(std::accumulate(co.begin(), co.end(), 0.) == n);
  return co;
}

std::tuple<std::vector<int>, std::vector<Real>> chi2testtable(const Real* x, const int n, Real alpha) {
  // Compute and cache c.
  static Real old_alpha = -1;
  static Real c;
  if (old_alpha != alpha) {
    c = std::sqrt(2) * erfinv(1. - 2 * alpha);
    old_alpha = alpha;
  }

  Real ks = n / 5.;
  Real kp = 4 * std::pow(2 * SQR((n - 1) / c), 1. / 5.);

  // for contingency tables see
  // [Bagnato, Punzo, & Nicolis, J. Time Ser. Anal., 33]
  //  if constexpr (multivariate) {
  //    ks = sqrt(ks);
  //    kp = sqrt(kp);
  //  }
  const int k = std::max(1, int(std::min(ks, kp)));  // number of bins
  // put data into bins
  // TODO: consider sorting and storing once.
  std::vector<Real> xu(n);
  std::copy_n(x, n, xu.begin());
  std::sort(xu.begin(), xu.end());

  std::vector<Real> be(k + 1, 0);  // Bins separators.
  be.front() = 0;                  // xu.front();
  assert(xu.front() >= be.front());
  be.back() = 1;  // xu.back();
  assert(xu.back() <= be.back());
  for (int j = 1; j < k; ++j) {  // loop over bins except last and first.
    int ns = round(static_cast<Real>(j) * static_cast<Real>(n) /
                   k);  // index of last sample in current bin
    be[j] = (xu[ns] + xu[ns + 1]) / 2.;
    // TODO: compute histogram count from here.
  }

  auto co = histcounts(x, be, n);

  return std::make_tuple(co, be);
}
