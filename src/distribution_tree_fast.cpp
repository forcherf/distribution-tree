#include "distribution_tree.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <tuple>
#include <stack>
#include <algorithm>
#include <numeric>

#include "split.hpp"
#include "util/container_to_stream.hpp"

namespace fast {

template <FitMode fit_mode, SplitMode split_mode>
void DistributionTree<fit_mode, split_mode>::construct(Matrix<Real>& data, Real alpha_fit,
                                                       Real alpha_indep) {
  data_ = std::move(data);
  const int d = data_.nrRows();
  const int n = data_.nrCols();

  slope_.resize(d);
  sums_.resize(d);
  x_sorted_.resize(n);

  const auto volume = normalize(data_);

  elements_.emplace_back();
  auto& root = elements_[0];

  root.density = volume;

  root.x = MatrixView<Real>(data_, n, 0);

  std::stack<int> todoes;  // Indices of Elements to process.
  todoes.push(0);

  while (!todoes.empty()) {
    const int node_idx = todoes.top();
    todoes.pop();
    auto* node = &elements_[node_idx];

    std::array<int, 2> tosplit{node->to_split, -1};

    if (node->x.nrCols() == 0) {
      node->to_split = tosplit[0] = -1;
      if constexpr (fit_mode == LINEAR)
        std::fill(slope_.begin(), slope_.end(), 0);
    }
    else if (node->to_split == -1)
      tosplit = fast::dimstosplit<fit_mode>(node->x, alpha_fit, alpha_indep, slope_, sums_, ks_,
                                            co2d_, x_sorted_, be_, co_);

    if (tosplit[0] != -1) {
      node->to_split = tosplit[0];

      elements_.emplace_back();
      elements_.emplace_back();

      node = &elements_[node_idx];  // In case of reallocation of the vector of elements.
      node->child1 = elements_.size() - 2;
      auto* child1 = &elements_[node->child1];
      node->child2 = elements_.size() - 1;
      auto* child2 = &elements_[node->child2];

      Real split_point;
      std::tie(child1->x, child2->x, split_point) = fast::split(node->to_split, node->x, split_mode);

      node->split_point = split_point;

      child1->density = node->density * split_point;
      child2->density = node->density * (1. - split_point);

      if (tosplit[1] != -1) {
        child1->to_split = tosplit[1];
        child2->to_split = tosplit[1];
      }

      todoes.push(node->child2);
      todoes.push(node->child1);
    }
    else {  // Node is a leaf.  Prepare fit parameters for query.
      // Replace the element volume with the density.
      node->density = node->x.nrCols() / (node->density * n);

      if constexpr (fit_mode == LINEAR) {  // Linear
        node->slope = slope_;
      }
    }
  }
}

// Note: only uniform mode is implemented.
template <FitMode fit_mode, SplitMode split_mode>
Real DistributionTree<fit_mode, split_mode>::query(Real* x) const {
  const int d = lb_.size();
  assert(d == lb_.size() && d == ub_.size());

  // Normalize input x.
  for (int i = 0; i < d; ++i) {
    if (x[i] < lb_[i] || x[i] > ub_[i])
      return 0;

    x[i] = (x[i] - lb_[i]) / (ub_[i] - lb_[i]);
    assert(0 <= x[i] || 1 >= x[i]);
  }

  const Element* node = &elements_[0];  // Root.
  // Find element
  while (node->child1 != -1) {
    const Real cut = node->split_point;
    const int dim = node->to_split;

    if (x[dim] <= cut) {  // First child.
      x[dim] /= cut;
      node = &elements_[node->child1];
    }
    else {  // Second child.
      x[dim] = (x[dim] - cut) / (1. - cut);
      node = &elements_[node->child2];
    }
  }

  if constexpr (fit_mode == UNIFORM) {
    return node->density;
  }
  else {
    Real prob = node->density;
    const auto& slope = node->slope;
    for (int i = 0; i < d; ++i) {
      const Real offset = (1 - 0.5 * slope[i]);
      prob *= offset + slope[i] * x[i];
    }
    return prob;
  }
}

template <FitMode fit_mode, SplitMode split_mode>
Real DistributionTree<fit_mode, split_mode>::normalize(Matrix<Real>& x) {
  lb_.resize(x.nrRows());
  ub_.resize(x.nrRows());
  Real volume = 1.;

  for (int i = 0; i < x.nrRows(); ++i) {
    lb_[i] = *std::min_element(x.row(i), x.row(i) + x.nrCols());
    ub_[i] = *std::max_element(x.row(i), x.row(i) + x.nrCols());
    volume *= ub_[i] - lb_[i];

    for (int j = 0; j < x.nrCols(); ++j) {
      x(i, j) = (x(i, j) - lb_[i]) / (ub_[i] - lb_[i]);
    }
  }

  return volume;
}

template <FitMode fit_mode, SplitMode split_mode>
bool DistributionTree<fit_mode, split_mode>::compare(
    const ::DistributionTree<fit_mode, split_mode>& other, Real tolerance) const {
  for (int i = 0; i < lb_.size(); ++i) {
    if (std::abs(lb_[i] - other.lb_[i]) > tolerance || std::abs(ub_[i] - other.ub_[i]) > tolerance)
      return false;
  }

  if (elements_.size() != other.elements_.size())
    return false;

  for (int i = 0; i < elements_.size(); ++i) {
    if (elements_[i].to_split != other.elements_[i].to_split)
      return false;
    if (elements_[i].child1 != other.elements_[i].child1)
      return false;
    if (std::abs(elements_[i].split_point - other.elements_[i].split_point) > tolerance)
      return false;
  }

  return true;
}

// template class DistributionTree<UNIFORM, MEDIAN>;
template class DistributionTree<LINEAR, MEDIAN>;
// template class DistributionTree<UNIFORM, HALF>;
template class DistributionTree<LINEAR, HALF>;

}  // namespace fast
