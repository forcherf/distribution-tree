#!/bin/bash

set +e # Disable exit on errors
#python3 make_perf_plots.py ./build/test/performance_tests/small_performance_test       > outps.txt;
#python3 make_perf_plots.py ./build/test/performance_tests/uniform_performance_test     > outpu.txt;
python3 make_speedup_light.py ./build/test/performance_tests/small_performance_test    2>&1 >outsls.txt;
python3 make_speedup_light.py ./build/test/performance_tests/uniform_performance_test  2>&1 >outslu.txt;
python3 make_fpc_plots_fast.py ./build/test/performance_tests/small_performance_test   2>&1 >outfs.txt;
python3 make_fpc_plots_fast.py ./build/test/performance_tests/uniform_performance_test 2>&1 >outfu.txt;
#python3 make_speedup.py ./build/test/performance_tests/small_performance_test          2>&1 >outss.txt;
#python3 make_speedup.py ./build/test/performance_tests/uniform_performance_test        2>&1 >outsu.txt;
set -e # Enable exit on error