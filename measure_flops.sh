#! /bin/bash

usage()
{
    echo "
Usage:
    measure_flops [-h | --help] | [-v | --verbose] -e file_name -n ndata -d dims -s seed

Notes:
    This script measures the flops of an executable, using an emulator.
    You have to put
        __SSC_MARK(0xFACE);
    in the code when starting measurements, and
        __SSC_MARK(0xDEAD);
    when stopping (the macro is defined in include/typedefs.hpp)

    **The macro does not influence performance!** So best to just leave them there.
    On the other hand calculating timing requires another run on a real CPU,
    since the program run in an emulator here.

    Run from this folder (i.e. 'bash ./measure_flops.sh').

Flags:
    -h | --help:      This help
    -v | --verbose:   Print all commands and all analysis of flops
    -e | --exec file: Executable file name, Expected same interface of multidim_gaussian
    -n | --ndata num: Number of d-dimensional datapoints
    -d | --ndims num: Number of dimensions
    -s | --seed  num: Seed number for the rng

Example:
    $ bash ./measure_flops.sh -v -e './build/applications/multidim_gaussian 1000 10'
"
}

verbose=0
execfile=./build/applications/multidim_gaussian



while [ "$1" != "" ]; do
    case $1 in
        -v | --verbose )        verbose=1
                                ;;
        -e | --exec )           shift
                                execfile="$1"
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

PATH=./other/intel-sde:$PATH

if [ $verbose = 1 ]
then
set -x
sde64 -iform -mix -dyn_mask_profile \
    -start_ssc_mark FACE:repeat -stop_ssc_mark DEAD:repeat -- \
    $execfile;
python ./intel_sde_flops.py sde-mix-out.txt sde-dyn-mask-profile.txt
set +x
else
sde64 -iform -mix -dyn_mask_profile \
    -start_ssc_mark FACE:repeat -stop_ssc_mark DEAD:repeat -- \
    $execfile >/dev/null;
python ./intel_sde_flops.py sde-mix-out.txt \
       sde-dyn-mask-profile.txt | grep tot_flops | cut -d: -f2
fi
