# distribution-tree

Group project for the ETHZ course How To Write Fast Numerical Code (Spring 2019).
Topic: Fast C/C++ implementation  of "Density estimation with distribution element trees" (https://arxiv.org/abs/1610.00345).