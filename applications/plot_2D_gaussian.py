#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

fig_id = 0

for dim in ['x', 'y'] :
    plt.figure(fig_id)

    data = np.loadtxt('estimate_' + dim + '.txt')
    norm = stats.norm.pdf(0) if dim =='y' else (stats.norm.pdf(0-2) + stats.norm.pdf(0+2))/2.

    plt.plot(data[:, 0], data[:, 1] / norm, color = 'red')
    generated = np.loadtxt('data2D.txt')

    x = np.linspace(np.min(data[:, 0]), np.max(data[:, 0]), 200)
    y = stats.norm.pdf(x) if fig_id == 0 else (stats.norm.pdf(x-2) + stats.norm.pdf(x+2))/2.

    plt.plot(x, y, color='blue')
    plt.title("Dimension " + dim)

    generated = np.loadtxt('data2D.txt')
    bins = int(np.sqrt(generated.shape[0]))
    plt.hist(generated[:, fig_id], bins, normed = 1)

    fig_id += 1

plt.show()
