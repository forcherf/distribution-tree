#include "distribution_tree.hpp"

#include <iostream>
#include <fstream>
#include <random>

// Generate and estimate for samples drawn from a normal distribution.
// Usage: [n datapoints] [n estimates in [-3, 3]]
int main(int argc, char** argv) {
  const int n = argc > 1 ? std::atoi(argv[1]) : 50000;
  const int n_samples = argc > 2 ? std::atoi(argv[2]) : 1000;
  constexpr int d = 1;
  std::mt19937_64 rng(42);
  std::normal_distribution<Real> normal(0., 1.);

  std::cout << "Generating data." << std::endl;
  Matrix<Real> data(1, n);
  std::ofstream datafile("data.txt");
  for (int i = 0; i < n; ++i) {
    data(0, i) = normal(rng);
    datafile << data(0, i) << "\n";
  }
  datafile.close();

  std::cout << "Fitting the tree." << std::endl;
  DistributionTree<LINEAR, MEDIAN> tree(data, 0.001, 0.001);

  std::cout << "Computing estimates." << std::endl;
  std::vector<Real> x_vec(1);
  std::ofstream out("estimate.txt");
  const Real dx = 6. / n_samples;
  for (Real x = -3; x <= 3; x += dx) {
    x_vec[0] = x;
    const Real pdf = tree.query(x_vec);
    out << x << "\t" << pdf << "\n";
  }
  out.close();
}
