#include "distribution_tree.hpp"

#include <iostream>
#include <fstream>
#include <random>

// Generate and estimate for samples drawn from a normal distribution.
// Usage: [n datapoints] [n estimates in [-3, 3]]
int main(int argc, char** argv) {
  const int n = argc > 1 ? std::atoi(argv[1]) : 50000;
  const int n_samples = argc > 2 ? std::atoi(argv[2]) : 1000;
  constexpr int d = 2;
  std::mt19937_64 rng(42);
  std::normal_distribution<Real> normal(0., 1.);
  std::uniform_int_distribution<int> coin(0, 1);

  std::cout << "Generating data." << std::endl;
  Matrix<Real> data(d, n);
  std::ofstream datafile("data2D.txt");
  for (int i = 0; i < n; ++i) {
    data(0, i) = normal(rng);
    data(1, i) = normal(rng) + 4 * coin(rng) - 2;
    datafile << data(0, i) << "\t" << data(1, i) << "\n";
  }
  datafile.close();

  std::cout << "Fitting the tree." << std::endl;
  fast::DistributionTree<LINEAR, MEDIAN> tree(data, 0.01, 0.01);

  std::cout << "Computing estimates." << std::endl;
  std::vector<Real> x_vec(d, 0);
  {
    std::ofstream out("estimate_x.txt");
    const Real dx = 6. / n_samples;
    for (Real x = -3; x <= 3; x += dx) {
      x_vec[0] = x;
      x_vec[1] = 0;
      const Real pdf = tree.query(x_vec);
      out << x << "\t" << pdf  << "\n";
    }
  }
  {
    std::ofstream out("estimate_y.txt");
    const Real dy = 10. / n_samples;
    for (Real y = -5; y <= 5; y += dy) {
      x_vec[0] = 0;
      x_vec[1] = y;
      const Real pdf = tree.query(x_vec);
      out << y << "\t" << pdf  << "\n";
    }
  }
}
