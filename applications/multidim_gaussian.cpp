#include "distribution_tree.hpp"
#include "profiling/profiler.hpp"
#include "util/generate_data.hpp"

#include <iostream>
#include <fstream>
#include <random>

constexpr bool print_data_and_tree = false;

// Generate and estimate for samples drawn from a normal distribution.
// Usage: [n datapoints] [n estimates in [-3, 3]]
int main(int argc, char** argv) {
  const int n = argc > 1 ? std::atoi(argv[1]) : 100000;
  const int d = argc > 2 ? std::atoi(argv[2]) : 100;
  const unsigned long seed = argc > 3 ? std::atoi(argv[3]) : 42;

  std::cout << "Generating data." << std::endl;
  Matrix<Real> data = generateData(n, d, seed);

  std::cout << "Fitting the tree." << std::endl;
  DistributionTree<UNIFORM, MEDIAN> tree;

  Profiler::start();
  {
    __SSC_MARK(0xFACE);
    Profiler prof("Tree fit", "", __LINE__);
    tree.construct(data, 0.01, 0.01);
    __SSC_MARK(0xDEAD);
  }
  Profiler::stop("profile.txt");

  if constexpr (print_data_and_tree) {
    std::ofstream datafile("data.txt");
    for (int j = 0; j < d; j++) {
      for (int i = 0; i < n - 1; i++) {
        datafile << data(j, i) << ",";
      }
      datafile << data(j, n - 1);  // here stamp without last comma
      datafile << "\n";
    }

    tree.write("density_tree.txt");
  }
}
