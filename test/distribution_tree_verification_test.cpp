#include "distribution_tree.hpp"

#include "gtest/gtest.h"

#include "util/generate_data.hpp"

#include <random>

constexpr bool update_baseline = false;

//TEST(DistributionTreeTest, UniformMedian) {
//  // Generate data.
//  const int n = 1000;
//  const int d = 8;
//
//  Matrix<Real> data = generateData(n, d);
//
//  DistributionTree<UNIFORM, MEDIAN> tree(data);
//  const std::string filename = SOURCE_DIR "/test/baseline_uniform_median.txt";
//
//  if constexpr (update_baseline)
//    tree.write(filename);
//  else {
//    DistributionTree<UNIFORM, MEDIAN> tree_check;
//    tree_check.read(filename);
//    EXPECT_TRUE(tree.compare(tree_check, 1e-4));
//  }
//}
//
//TEST(DistributionTreeTest, LinearMedian) {
//  // Generate data.
//  const int n = 1259;
//  const int d = 7;
//
//  Matrix<Real> data = generateData(n, d);
//
//  auto swap = [](int i, int j, auto& x){
//    for(int l = 0; l < x.nrRows(); ++l)
//      std::swap(x(l, i), x(l, j));
//  };
//
//  std::mt19937_64 rng(0);
//  std::uniform_int_distribution<int> distro(0, n-1);
//
//  for(int i = 0; i < n; ++i){
//    swap(distro(rng), distro(rng), data);
//  }
//
//  DistributionTree<LINEAR, HALF> tree(data);
//  const std::string filename = SOURCE_DIR "/test/baseline_linear_half.txt";
//
//  if constexpr (update_baseline)
//    tree.write(filename);
//  else {
//    DistributionTree<LINEAR, HALF> tree_check;
//    tree_check.read(filename);
//    EXPECT_TRUE(tree.compare(tree_check, 1e-4));
//  }
//}

TEST(DistributionTreeTest, FastTree) {
  // Generate data.
  const int n = 5000;
  const int d = 4;

  Matrix<Real> data = generateData(n, d);
  Matrix<Real> data2 = data;

  DistributionTree<LINEAR, HALF> tree(data);
  fast::DistributionTree<LINEAR, HALF> tree_fast(data2);

  std::vector<Real> point(d), point_cpy(d);
  std::mt19937_64 rng(0);

  const int grid_l = 10;
  std::vector<Real> dx(d), x(d);
  for (int i = 0; i < d; ++i)
    dx[i] = (tree.upperBound()[i] - tree.lowerBound()[i]) / (grid_l);
  Matrix<Real> grid(std::pow(grid_l, 4), d, 0);

  int grid_id = 0;
  for (x[0] = tree.lowerBound()[0] + dx[0] / 2.; x[0] < tree.upperBound()[0]; x[0] += dx[0])
    for (x[1] = tree.lowerBound()[1] + dx[1] / 2.; x[1] < tree.upperBound()[1]; x[1] += dx[1])
      for (x[2] = tree.lowerBound()[2] + dx[2] / 2.; x[2] < tree.upperBound()[2]; x[2] += dx[2])
        for (x[3] = tree.lowerBound()[3] + dx[3] / 2.; x[3] < tree.upperBound()[3]; x[3] += dx[3]) {
          std::copy_n(x.data(), d, grid.row(grid_id));
          ++grid_id;
        }

  auto L2_distance = [](const std::vector<Real>& x1,
                        const std::vector<Real>& x2) -> std::tuple<Real, Real> {
    assert(x1.size() == x2.size());
    Real norm = 0, dist = 0;
    for (int i = 0; i < x2.size(); ++i) {
      dist += (x1[i] - x2[i]) * (x1[i] - x2[i]);
      norm += (x1[i]) * (x1[i]);
    }
    return std::make_tuple(std::sqrt(dist / x1.size()), std::sqrt(norm / x1.size()));
  };

  Matrix<Real> grid_cpy(grid);
  const auto expected = tree.query(grid_cpy);
  const auto optimized = tree_fast.query(grid);

  auto [diff, norm] = L2_distance(expected, optimized);

  std::cout << "Diff L2: " << diff << std::endl;
  std::cout << "Norm L2: " << norm << std::endl;
  EXPECT_LT(diff, 1e-3);
  EXPECT_LT(diff / norm, 1e-1);
}
