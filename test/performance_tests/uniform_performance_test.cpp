#include "tsc_x86.h"
#include "timer.hpp"

#include "distribution_tree.hpp"
#include "profiling/profiler.hpp"
#include "util/generate_data.hpp"
#include "util/counters.hpp"

#include <iostream>

//

// Generate and estimate for samples drawn from a normal distribution.
// Usage ./small_performance [run_slow = true]
int main(int argc, char** argv) {
  const int n = argc > 1 ? std::atoi(argv[1]) : 1000000;
  const int d = argc > 2 ? std::atoi(argv[2]) : 128;
  const bool run_slow = argc > 3 ? std::atoi(argv[3]) : false;

  std::cout << "Generating data." << std::endl;
  Matrix<Real> data = generateUniform(n, d);

  auto benchmark = [&](auto& tree) {
    auto data_copy(data);
    std::cout << "Fitting the tree." << std::endl;
    counters::flop = 0;
    startTimer();
    auto cycle_start = start_tsc();

    // HeapProfilerStart("/tmp/small_pt.hprof");
    __SSC_MARK(0xFACE);
    tree.construct(data_copy, 1e-4, 1e-2);
    __SSC_MARK(0xDEAD);
    // HeapProfilerDump("End tree construction");
    // HeapProfilerStop();

    auto cycle_delta = stop_tsc(cycle_start);
    auto time = endTimer();

    std::cout << "Elements: " << tree.size() << std::endl;
    std::cout << "Time:" << time << std::endl;
    std::cout << "Cycles:" << cycle_delta << std::endl;
    std::cout << "FLOPs:" << counters::flop << std::endl;
    std::cout << "FLOP/cycle:" << double(counters::flop) / cycle_delta << std::endl;
  };

  if (run_slow) {
    std::cout << "Slow: " << std::endl;
    DistributionTree<LINEAR, HALF> tree;
    benchmark(tree);
    std::cout << std::endl << std::endl;
  } else {
    std::cout << "Fast" << std::endl;
    fast::DistributionTree<LINEAR, HALF> tree;
    benchmark(tree);
  }
}
