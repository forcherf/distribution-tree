#pragma once

#include <chrono>

namespace {
std::chrono::high_resolution_clock::time_point __start;
}

void startTimer() {
  __start = std::chrono::high_resolution_clock::now();
}

double endTimer() {
  auto end = std::chrono::high_resolution_clock::now();
  auto time_span = std::chrono::duration_cast<std::chrono::duration<double>>(end - __start);
  return time_span.count();
}
