#include "tsc_x86.h"
#include "timer.hpp"

#include "distribution_tree.hpp"
#include "profiling/profiler.hpp"
#include "util/generate_data.hpp"

#include <iostream>

// Generate and estimate for samples drawn from a normal distribution.
int main(int argc, char** argv) {
  const int n = 500000;
  const int d = 512;

  std::cout << "Generating data." << std::endl;
  Matrix<Real> data = generateData(n, d);

  std::cout << "Fitting the tree." << std::endl;
  startTimer();

  auto cycle_start = start_tsc();
  fast::DistributionTree<LINEAR, HALF> tree(data, 0.01, 0.01);
  auto cycle_delta = stop_tsc(cycle_start);
  auto time = endTimer();

  std::cout << "Cycles " << cycle_delta << std::endl;
  std::cout << "Time\t" << time << std::endl;
}
