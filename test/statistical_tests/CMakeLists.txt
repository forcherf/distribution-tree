# Statistcical tests

add_gtest(chi_squared_cdf_test LIBS statistical_tests gsl)

add_gtest(util_test
          LIBS statistical_tests)

add_gtest(independence_test_test
          LIBS statistical_tests)

add_gtest(chi_squared_pvalue_test
          LIBS statistical_tests)

add_gtest(find_statistic_test
  LIBS statistical_tests)

