#include "statistical_tests/independence_test.hpp"

#include "statistical_tests/histograms.hpp"

#include <random>

#include "gtest/gtest.h"

constexpr double tolerance = 5e-7;

TEST(IndependenceTestTest, AreDependent) {
  const int n = 200;
  const int dim = 2;  // 2D data
  Matrix<Real> x(dim, n);

  std::mt19937_64 rng(0);
  std::uniform_real_distribution<Real> distro(0, 1);

  for (int i = 0; i < n; ++i) {
    x(0, i) = distro(rng);
    x(1, i) = x(0, i) * 0.5 + 0.5 * distro(rng);
  }

  auto [res, pval] = chi2IndepTest(x, 0.05);

  for (int i = 0; i < dim; ++i)
    for (int j = 0; j < dim; ++j) {
      // Note: true means dependant.
      EXPECT_EQ(res(i, j), true);
    }
}

TEST(IndependenceTestTest, AreNotDependent) {
  const int n = 37;
  const int dim = 2;  // 2D data
  Matrix<Real> x(dim, n);

  std::mt19937_64 rng(0);
  std::uniform_real_distribution<Real> distro(0, 1);

  for (int i = 0; i < n; ++i) {
    x(0, i) = distro(rng);
    x(1, i) = distro(rng);
  }

  auto [res, pval] = chi2IndepTest(x, 0.05);

  for (int i = 0; i < dim; ++i)
    for (int j = 0; j < dim; ++j) {
      // Note: true means dependant.
      EXPECT_EQ(res(i, j), i == j);
    }
}
