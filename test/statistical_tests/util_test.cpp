#include "statistical_tests/util.hpp"

#include "gtest/gtest.h"

#include <fstream>
#include <limits>
#include <string>

constexpr double tolerance = 10 * std::numeric_limits<Real>::epsilon();

TEST(UtilTest, Erfinv) {
  // Expected values computed with python.
  std::fstream inp(SOURCE_DIR "test/statistical_tests/python_erfinv.txt");
  std::vector<Real> xs, ys;

  while(inp){
    Real x, y;
    inp >> x >> y;
    xs.push_back(x);
    ys.push_back(y);
  }

  for (int i = 0; i < xs.size(); ++i)
    EXPECT_NEAR(ys[i], erfinv(xs[i]), tolerance);
}

TEST(UtilTest, ErfCinv) {
  EXPECT_NEAR(erfcinv(0.77), 1. - erfinv(0.77), tolerance);
}
