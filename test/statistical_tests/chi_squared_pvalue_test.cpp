#include "statistical_tests/chi_squared_pvalue_test.hpp"

#include <fstream>
#include <random>

#include <gtest/gtest.h>

TEST(ChiSquaredPvalueTestTest, Uniform) {
  std::mt19937_64 rng(0);
  std::uniform_real_distribution<Real> distro(0, 1);

  std::vector<Real> x(200);
  for (auto& val : x) {
    val = distro(rng);
  }

  auto [rejection_uni, p_uni] = chiSquaredUniformTest(x.data(), x.size(), 0.05);
  EXPECT_EQ(rejection_uni, false);

  auto [rejection_lin, pval, lin_pars] = chiSquaredLinearTest(x.data(), x.size(), 0.05);
  EXPECT_EQ(rejection_lin, false);
  EXPECT_NEAR(lin_pars[1], 0., 0.1);
}

TEST(ChiSquaredPvalueTestTest, Linear) {
  std::mt19937_64 rng(0);
  std::uniform_real_distribution<Real> distro(0, 1);

  std::vector<Real> x;
  for (int i = 0; i < 200; ++i) {
    const Real candidate = distro(rng);
      // linear distribution pdf = 2x.
      // cdf = x**2
      // cdf**-1 = sqrt(x)
      x.push_back(std::sqrt(distro(rng)));
  }

  auto [rejection_uni, p_uni] = chiSquaredUniformTest(x.data(), x.size(), 0.05);
  EXPECT_EQ(rejection_uni, true);

  auto [rejection_lin, pval, lin_pars] = chiSquaredLinearTest(x.data(), x.size(), 0.05);
  EXPECT_EQ(rejection_lin, false);
  EXPECT_NEAR(lin_pars[1], 2., 0.2);
}

TEST(ChiSquaredPvalueTestTest, Square) {
    std::mt19937_64 rng(0);
    std::uniform_real_distribution<Real> distro(0, 1);

    std::vector<Real> x;
    for (int i = 0; i < 200; ++i) {
        const Real candidate = distro(rng);
        // quadratic distribution pdf = 3x**2.
        // cdf = x**3
        // cdf**-1 = pow(x, 1./3)
        x.push_back(std::pow(distro(rng), 1./3.));
    }

    auto [rejection_uni, p_uni] = chiSquaredUniformTest(x.data(), x.size(), 0.05);
    EXPECT_EQ(rejection_uni, true);

    auto [rejection_lin, pval, lin_pars] = chiSquaredLinearTest(x.data(), x.size(), 0.05);
    EXPECT_EQ(rejection_lin, true);
}
