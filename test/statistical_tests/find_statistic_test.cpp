#include "statistical_tests/find_statistics.hpp"
#include <vector>

#include "gtest/gtest.h"

constexpr double tolerance = 5e-7;

TEST(FindStatisticTest, All) {
  std::vector<Real> data{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
  std::random_shuffle(data.begin(), data.end());
  std::vector<int> k{0, 1, 7, 10, 15};

  fast::findStatistics(data.data(), data.size(), k.data(), k.size());
  for (auto val : k)
    EXPECT_EQ(val, data[val]);
}
