#include "statistical_tests/chi_squared_cdf.hpp"

#include "gtest/gtest.h"

TEST(ChiSquaredCdfTest, ValidArgs) {
  // Expected values computed with python.
  constexpr double tolerance = 1e-7;
  EXPECT_NEAR(chiSquaredCdf(0, 2), 0, tolerance);
  EXPECT_NEAR(chiSquaredCdf(0.22186288677770544, 2), 0.10499989499999993, tolerance);
  EXPECT_NEAR(chiSquaredCdf(2.1558559589201485, 10), 0.0049999950000000851, tolerance);
  EXPECT_NEAR(chiSquaredCdf(2121, 2000), 0.970378888203006, tolerance);
}

TEST(ChiSquaredCdfTest, InvalidArgs) {
    EXPECT_ANY_THROW(chiSquaredCdf(-1, 1));
    EXPECT_ANY_THROW(chiSquaredCdf(1, -1));
}
