#include "split.hpp"

#include <random>
#include "gtest/gtest.h"

TEST(DimstosplitTest, Split) {
  constexpr int dim = 3;
  constexpr int n = 100;
  Matrix<Real> x(dim, n);

  // Create random data.
  std::mt19937_64 rng(42);
  std::uniform_int_distribution<int> coin(0, 1);
  std::uniform_real_distribution<Real> blob1(0, 0.33);
  std::uniform_real_distribution<Real> blob2(0.66, 1);
  std::uniform_real_distribution<Real> uniform(0, 1);
  for (int i = 0; i < n; ++i) {
    x(0, i) = coin(rng) == 1 ? blob2(rng) : blob1(rng);  // dim 0 is independent but not unform.
    x(1, i) = uniform(rng);                              // dim 1 and 2 are dependant.
    x(2, i) = 1 - x(1, i);
  }
  const std::vector<Real> sze(3, 1);

  {
    // Split due to non uniformity.
    auto [to_split, p] = dimstosplit<UNIFORM>(x, sze, 0.05, 0.05);
    EXPECT_EQ(to_split[0], 0);
    EXPECT_EQ(to_split[1], -1);
  }
  {
    // Split due to dependency.
    //    MatrixView<Real> x_part(x, 1, 3, 2, n - 3); // take last 2 dimensions and all data minus 3.
    Matrix<Real> x_part(2, n);
    for (int i = 0; i < 2; ++i)
      for (int j = 0; j < n; ++j) {
        x_part(i, j) = x(i + 1, j);
      }

    auto [to_split, p] = dimstosplit<UNIFORM>(x_part, sze, 0.05, 0.05);
    EXPECT_EQ(to_split.size(), 2);
    EXPECT_TRUE((to_split[0] == 0) || (to_split[0] == 1));
    EXPECT_TRUE((to_split[1] == 0) || (to_split[1] == 1));
    EXPECT_NE(to_split[0], to_split[1]);
  }
}

TEST(DimstosplitTest, NoSplit) {
  constexpr int dim = 5;
  constexpr int n = 500;
  Matrix<Real> x(dim, n);

  // Create random data.
  std::mt19937_64 rng(42);
  std::uniform_real_distribution<Real> uniform(0, 1);
  for (int d = 0; d < dim; ++d)
    for (int i = 0; i < n; ++i) {
      x(d, i) = uniform(rng);
    }

  const std::vector<Real> sze(dim, 1);

  auto [to_split, p] = dimstosplit<UNIFORM>(x, sze, 0.05, 0.05);
  EXPECT_EQ(to_split[0], -1);
  EXPECT_EQ(to_split[1], -1);
}

TEST(SplitTest, Fast) {
  constexpr int dim = 2;
  constexpr int n = 5;
  Matrix<Real> x(dim, n);

  x(0, 0) = 0.1, x(0, 1) = 0.75, x(0, 2) = 0.6, x(0, 3) = 0.1, x(0, 4) = 0.1;
  x(1, 0) = 1, x(1, 1) = 2, x(1, 2) = 2, x(1, 3) = 1, x(1, 4) = 1;

  MatrixView<Real> xview(x, n, 0);
  auto [x1, x2, split] = fast::split(0, xview, HALF);

  EXPECT_EQ(x1.nrCols(), 3);
  for(int i = 0; i < x1.nrCols(); ++i)
    EXPECT_EQ(1, x1(1, i));

  EXPECT_EQ(x2.nrCols(), 2);
  for(int i = 0; i < x2.nrCols(); ++i)
    EXPECT_EQ(2, x2(1, i));
}
