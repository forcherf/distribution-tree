#!/usr/bin/python3

import sys
import matplotlib.pyplot as plt
import numpy as np

from sklearn.datasets import make_classification
from sklearn.datasets import make_blobs
from sklearn.datasets import make_gaussian_quantiles


print('Number of arguments:', len(sys.argv), 'arguments.')
print('Argument List:', str(sys.argv))

ndata = int(sys.argv[1])
dimension = int(sys.argv[2])
nclusters = int(sys.argv[3])
dmax = float(sys.argv[4])
dmin = float(sys.argv[5])

clus_std = 1.0

X1, Y1 = make_blobs(n_samples=ndata, n_features=dimension, 
                    centers=None, cluster_std=clus_std, 
                    center_box=(-20.0, 20.0), shuffle=True, random_state=None)




plt.figure()
plt.title("Three blobs", fontsize='small')
plt.scatter(X1[:, 0], X1[:, 1], marker='o', #c=Y1,
            s=25, edgecolor='k')


np.savetxt("blob_data.txt", X1, fmt='%.18e', delimiter=' ', newline='\n', header='', footer='', comments='# ', encoding=None)


plt.show()


