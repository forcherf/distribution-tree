
#include <iostream>
#include <random>

#ifndef __SSC_MARK
#define __SSC_MARK(tag)                                                        \
        __asm__ __volatile__("movl %0, %%ebx; .byte 0x64, 0x67, 0x90 "         \
                             ::"i"(tag) : "%ebx")
#endif

using std::cout;

extern "C" {
    auto func_wrapper(double* __restrict__ x, double* __restrict__ y, double* __restrict__ z, int n) {

    double sum;
    // 2 adds 1 mult per N^2, 3N^2 flops
    // Compulsory: 2 Reads
    for (int i=0; i < n; i++) {
        for (int j = 0; j < n; j++)
        {
            z[i] += x[j] + y[j]*x[j];
        }
    }
    //cout << "Computed calc" << std::endl;
    //1 add per N iter
    //N reads
    for (int i = 0; i < n; i++)
    {
        sum += z[i];
    }

    return sum;
    }
}
/** 
 * Code to test flop counting script with Intel SDE emulator
 * https://github.com/It4innovations/Intel-SDE-FLOPS
 * How to use: 
 * 1) Download the Intel SDE Emulator, last time found at 
 *    https://software.intel.com/en-us/articles/pre-release-license-agreement-for-intel-software-development-emulator-accept-end-user-license-agreement-and-download
 *    Put somewhere and follow instructions,in particular:
 *      - Do $ sudo bash -c "echo 0 > /proc/sys/kernel/yama/ptrace_scope" to use the SDE
 *      - Add the folder to path (important for script)
 * 2) Clone https://github.com/It4innovations/Intel-SDE-FLOPS and
 *    get the intel_sde_flops.py script
 * 3) Run the SDE on the executable with
 *      $ sde64 -iform -mix -dyn_mask_profile -- ./a.out 10   
 *      to profile all flops. Use:
 *      $ sde64 -iform -mix -dyn_mask_profile -start_ssc_mark FACE:repeat -stop_ssc_mark DEAD:repeat -- ./a.out 10
 *    togheter with the __SSC_MARK macro (see python script's github readme) to
 *    measure only a single code section
 *  4) Run the script in the same folder (it should see the two sde-*.txt output from the emulator)
 *     and finally see the flops.
 */
int main(int argc, char *argv[]){
    std::mt19937_64 rng(93);
    std::normal_distribution<double> normal(0., 1.);


    const long long n = argc > 1 ? std::atoi(argv[1]) : 10;
    //const int d = argc > 2 ? std::atoi(argv[2]) : 100;

    double* x = new double[n];
    double* y = new double[n];
    double* z = new double[n];
    double sum = 0;

    //Fill arrays
    for (int i = 0; i < n; i++)
    {
        x[i]=normal(rng);
        y[i]=normal(rng);
    }
    cout << "Filled arrays" << std::endl;
    __SSC_MARK(0xFACE);
    sum = func_wrapper(x,y,z,n);
    __SSC_MARK(0xDEAD);
    
    cout << "Computed sum" << sum << std::endl;
    
    std::cout << "Expected 3N^2+N flops = " << 3*n*n+n << std::endl;
    std::cout << "Sum: " << sum << std::endl;

    return 0;
}