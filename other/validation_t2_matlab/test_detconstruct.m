function [M] = test_detconstruct(datain,mode,points,results)

data=readmatrix(datain);
det = detconstruct(data, mode);

testpoints=readmatrix(points);
testresults=readmatrix(results)

for k=1:5
  p(k) = detquery(det,testpoints(k,:)');
end
p

M=[p;testresults];
M

end
