function [t] = timing_detconstruct(datain,mode)

data=readmatrix(datain);
tic
det = detconstruct(data, mode);
t=toc;
t
end

