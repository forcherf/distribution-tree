#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

data = np.loadtxt('estimate.txt')

plt.plot(data[:, 0], data[:, 1], color = 'red')

x = np.linspace(np.min(data[:, 0]), np.max(data[:, 0]), 200)
y = stats.norm.pdf(x)

plt.plot(x, y, color='blue')

generated = np.loadtxt('data.txt')
bins = int(np.sqrt(len(generated)))
plt.hist(generated, bins, normed = 1)

plt.show()
