% extract characteristics of leaf or final elements of distribution element tree (det)
% det: distribution element tree struct resulting from detconstruct.m
% leafs: struct with leafs data
% d: number of sample space dimensions
% m: number of leaf elements in det
% example:
% x = mvnrnd([0 0],[4.0 -2.28; -2.28 1.44],1E5)';
% det = detconstruct(x);
% [leafs,d,m] = detleafs(det);
% figure, plot(x(1,1:100:end),x(2,1:100:end),'.r'), hold on
% for k = 1:m
%    p = leafs.lb(:,k); w = leafs.s(:,k);
%    xy = [p(1) p(1)+w(1) p(1)+w(1) p(1) p(1); p(2) p(2) p(2)+w(2) p(2)+w(2) p(2)];
%    xy = det.A' * xy + det.mu*ones(1,5);
%    plot(xy(1,:),xy(2,:),'-k')
% end
% axis equal, hold off
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, February 2017
function [leafs,d,m] = detleafs(det)
d = size(det.lb,1); % number of dimensions
indl = find(det.tree(:,2) <= 0); % indices of leafs in tree
m = length(indl); % number of leaf elements
% initialize leaf property fields
p = zeros(1,m); % de probability density
theta = cell(m,1); % de parameters
lb = zeros(d,m); % de lower bound
s = ones(d,m); % de size
div = zeros(d,m); % de subdivisions
% extract leaf data
for k = 1:m % loop over leaf elements
   [p(k),theta{k},lb(:,k),s(:,k),div(:,k)] = de(det, indl(k));
end
% return data
leafs = struct('p',p,'theta',{theta},'lb',lb,'s',s,'div',div);
end

