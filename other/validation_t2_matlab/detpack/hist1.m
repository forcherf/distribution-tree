% one-dimensional adaptive histogram based on distribution element tree (det)
% mode: > 0 equal-size splits (default), < 0 equal-score splits
% bounds: vector [lb ub], where lb and ub are lower and upper bounds of sample space (optional)
% h: histogram value
% bc, bw: bin centers and widths
% examples
%   Gaussian
% x = normrnd(0.5,3.0,1E4,1); figure, hist1(x);
% x1 = get(gca,'XLim'); x2 = x1(2); x1 = x1(1); b = x1 + (x2-x1)*([1:100]-0.5)/100;
% hold on, plot(b,normpdf(b,0.5,3.0),'k'), hold off
%   linear (with a peak)
% x = rand(2,1E6); x = x(1,x(2,:) <= x(1,:)); x = [x 0.25*ones(1,5E2)]; figure, hist1(x);
%   uniform
% x = rand(1E4,1)*0.9+0.6; figure, hist1(x,1,[0 2]);
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, February 2017
function [h,bc,bw] = hist1(x, mode, bounds)
% check format of input data x
if (~isvector(x))
   error(struct('identifier','hist1:inputError','message','Input data is not a vector.'))
end
x = reshape(x,1,length(x));
% check optional arguments
if (exist('mode','var')), mode = 2*(mode > 0)-1; else, mode = 1; end
% build det
if (exist('bounds','var'))
   if (any((x < bounds(1)) | (bounds(2) < x)))
      error(struct('identifier','hist1:inputError','message','Input data outside of given bounds.'))
   end
   det = detconstruct(x, mode, bounds(1), bounds(2));
else
   det = detconstruct(x, mode, 0, 0);
end
% extract histogram bin data
[leafs,~,m] = detleafs(det);
bc = leafs.lb + leafs.s/2; bw = leafs.s; h = leafs.p;
% plot histogram
color = [205 224 247]/255;
for k = 1:m
   rectangle('Position',[leafs.lb(k) 0 leafs.s(k) leafs.p(k)],'FaceColor',color), hold on
end
axis auto, hold off
end
