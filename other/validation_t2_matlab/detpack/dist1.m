% one-dimensional piecewise linear pdf estimator based on distribution element tree (det)
% mode: > 0 equal-size splits (default), < 0 equal-score splits
% bounds: vector [lb ub], where lb and ub are lower and upper bounds of sample space (optional)
% h: left and right bin values
% bc, bw: bin centers and widths
% examples;
%   Gaussian
% x = normrnd(0.5,3.0,1E4,1); figure, dist1(x);
% x1 = get(gca,'XLim'); x2 = x1(2); x1 = x1(1); b = x1 + (x2-x1)*([1:100]-0.5)/100;
% hold on, plot(b,normpdf(b,0.5,3.0),'k'), hold off
%   linear (with a peak)
% x = rand(2,1E6); x = x(1,x(2,:) <= x(1,:)); x = [x 0.25*ones(1,5E2)]; figure, dist1(x);
%   uniform
% x = rand(1E4,1)*0.9+0.6; figure, dist1(x,1,[0 2]);
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, February 2017
function [h,bc,bw] = dist1(x, mode, bounds)
% check format of input data x
if (~isvector(x))
   error(struct('identifier','dist1:inputError','message','Input data is not a vector.'))
end
x = reshape(x,1,length(x));
% check optional arguments
if (exist('mode','var')), mode = 4*(mode > 0)-2; else, mode = 2; end
% build det
if (exist('bounds','var'))
   if (any((x < bounds(1)) | (bounds(2) < x)))
      error(struct('identifier','dist1:inputError','message','Input data outside of given bounds.'))
   end
   det = detconstruct(x, mode, bounds(1), bounds(2));
else
   det = detconstruct(x, mode, 0, 0);
end
% extract bin data
[leafs,~,m] = detleafs(det);
bc = leafs.lb + leafs.s/2; bw = leafs.s;
h = zeros(m,2);
for k = 1:m
   h(k,:) = leafs.p(k);
   if (h(k,1) > 0), h(k,:) = h(k,:) .* ([-1 1]*leafs.theta{k}/2 + 1); end
end
% plot piecewise linear pdf
color = [205 224 247]/255;
for k = 1:m
   fill(bc(k)+[-1 +1 +1 -1]*bw(k)/2,[0 0 h(k,2) h(k,1)],color), hold on
end
axis auto, hold off
end
