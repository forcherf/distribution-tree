% generate (un)conditional random numbers from distribution element tree (det)
% det: distribution element tree struct resulting from detconstruct.m
% xc, d: column vectors with conditioning planes located along dimensions d at positions xc
%        set xc = d = [] or ignore for unconditional random number generation
% n: number of samples x to generate
%   2d examples:
% n = 1E5; x1c = 0.5; x2c = 0.2;
% %mu = [0 0]; C = [4.0 -2.28; -2.28 1.44];
% %x = mvnrnd(mu,C,n)';
% %x = 3*rand(2,n); x = x(1,x(2,:) <= x(1,:)); x(1,:) = sqrt(x(1,:)); x(2,:) = rand(1,size(x,2));
% x = [rand(2,n/2) ([5; 0]*ones(1,n/2) + [0.2 0; 0 4]*rand(2,n/2)) [-2.2 6.2; -1.4 6.4]];
% det = detconstruct(x,2,0,0); y = detrnd(det, n);
% figure, dist2(x); xlabel('x_1'), ylabel('x_2'), title('original')
% figure, dist2(y); xlabel('x_1'), ylabel('x_2'), title('sampled')
% y = detrnd(det, n, x1c, 1);
% figure, dist1(y(2,:)); xlabel('x_2'), title(['f(x_2|x_1 = ' num2str(x1c) ')'])
% x2r = det.lb(2) + (det.ub(2)-det.lb(2))*([1:100]-0.5)/100;
% %y2r = mvnpdf([x1c*ones(100,1) x2r'],mu,C);
% y2r = detquery(det,[x1c*ones(1,100); x2r]);
% y2r = y2r/((x2r(2)-x2r(1))*sum(y2r)); hold on, plot(x2r,y2r,'-r'), hold off
% y = detrnd(det, n, x2c, 2);
% figure, dist1(y(1,:)); xlabel('x_1'), title(['f(x_1|x_2 = ' num2str(x2c) ')'])
% x1r = det.lb(1) + (det.ub(1)-det.lb(1))*([1:100]-0.5)/100;
% %y1r = mvnpdf([x1r' x2c*ones(100,1)],mu,C);
% y1r = detquery(det,[x1r; x2c*ones(1,100)]);
% y1r = y1r/((x1r(2)-x1r(1))*sum(y1r)); hold on, plot(x1r,y1r,'-r'), hold off
%   3d examples:
% n = 1E5; x1c = 0.7;
% x = zeros(3,n); x(1,:) = normrnd(0, 0.5 ,1 ,n);
% x(2,:) = x(1,:) + normrnd(0, 0.25, 1, n); x(3,:) = x(1,:) + x(2,:) + normrnd(0, 0.1, 1, n);
% %x = rand(3, n); x = x(:,x(1,:)+x(2,:)+x(3,:) < 1); n = size(x,2);
% plot3(x(1,:),x(2,:),x(3,:),'.k'), axis equal, xlabel('x_1'), ylabel('x_2'), zlabel('x_3')
% x21 = min(x(2,:)); x22 = max(x(2,:)); x31 = min(x(3,:)); x32 = max(x(3,:));
% patch(x1c*ones(1,4),[x21 x22 x22 x21],[x31 x31 x32 x32],'w','FaceAlpha',0.9), grid on
% det = detconstruct(x,2,0,0);
% y = detrnd(det, n, x1c, 1);
% figure, dist2(y(2:3,:)); xlabel('x_2'), ylabel('x_3'), title(['f(x_2,x_3|x_1 = ' num2str(x1c) ')'])
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, December 2018
function x = detrnd(det, n, xc, d)
if (~exist('xc','var')), xc = []; d = []; end
% collect det leafs cut by condit'g planes p
if (isempty(xc)) % unconditional case
   leafs = find(det.tree(:,2) == 0); % list with indices of all elements
else % conditional case
   if (~(all(all(det.A == eye(size(det.lb,1)))) && all(det.mu == 0)))
      error(struct('identifier','detrnd:inputError','message',...
         'Principle axes transform is not supported when conditioning (det.A is not identity matrix, use det = detconstruct(data, mode, 0, 0)).'))
   end
   leafs = detcut(det, xc, d); % list with indices of cut elements
end
if (isempty(leafs)), x = []; return, end % condit'g planes outside sample space
% extract leaf properties
ds = size(det.lb,1); % number of sample space dimensions
m = length(leafs); % number of cut leafs
p = zeros(m,1); % de probability density
theta = cell(m,1); % de parameters
lb = zeros(ds,m); % de lower bound
s = ones(ds,m); % de size
for k = 1:m % loop over cut leafs
   [p(k),theta{k},lb(:,k),s(:,k),~] = de(det, leafs(k));
end
% determine (conditional) leaf probabilites
sl = zeros(ds,m); % slope parameter
for k = 1:m, if (length(theta{k}) == ds), sl(:,k) = theta{k}; end, end
p = p.*prod(s,1)' .* prod((((xc*ones(min(1,length(d)),m)-lb(d,:))./s(d,:) - 1/2).*sl(d,:) + 1) ./ s(d,:), 1)';
% generate random samples
p = cumsum(p);
if (p(end) == 0), x = []; return, end % zero probability
p = [0; p/p(end)]; % normalize probabilities
x = zeros(ds,n);
parfor k = 1:n % loop over samples
   % randomly select one leaf based on leaf probabilities
   r = rand(1); if (r == 0), r = 1; end
   ind = (p(1:end-1) < r) & (r <= p(2:end));
   % randomly draw sample inside selected leaf with inverse marginal cdfs
   for dim = 1:ds % loop over sample space dimensions
      j = find(d == dim); % index of dim in condit'g list
      if (isempty(j)) % dim is not condit'g direction
         x0 = lb(dim,ind); x1 = lb(dim,ind)+s(dim,ind);
         sl = theta{ind}(dim);
         q = rand(1);
         if (abs(sl) < 1E-6) % constant de with uniform pdf
            x(dim,k) = x0 + (x1-x0)*q;
         else % linear de with linear pdf
            % generate sample from inverse cdf derived in linear_distribution.nb
            a = sqrt(4 + sl*(sl+8*q-4));
            x(dim,k) = 1/(2*sl) * ((2+sl-a)*x0 + (-2+sl+a)*x1);
         end
      else % dim is condit'g direction
         x(dim,k) = xc(j); % assign conditional value
      end
   end
end
% in unconditional case, transform samples back
if (isempty(xc)), x = det.A'*x + det.mu*ones(1,n); end
end



% identify det leafs that are cut by condit'g planes
function leafs = detcut(det, p, d)
leafs = []; m = 0; % list and counter of cut det leafs
p = (p - det.lb(d)) ./ (det.ub(d) - det.lb(d)); % p in [0,1]
% no leafs are cut with condit'g planes outside of sample space
if (any((p < 0) | (1 < p))), return, end
% initialize list of interim elements for search of cut final elements
ind = zeros(size(det.tree,1),1); n = 0; % list and counter of interim elements
pr = zeros(length(p),size(det.tree,1)); % relative condit'g plane positions
ind(1) = 1; n = 1; pr(:,1) = p; % start from root de
% search det level-by-level for leafs that are cut by condit'g planes
leafs = zeros(size(det.tree,1),1);
while (n > 0) % loop until all interim elements were searched
   nc = n; % current number of interim elements to be checked
   k = 1; % counter for ind list
   while (k <= nc) % loop over interim elements
      % is element a tree leaf or final element?
      if (det.tree(ind(k),2) == 0) % yes, final
         % add to list of cut det leafs
         m = m+1; leafs(m) = ind(k);
         % remove leaf from list of leafs to be checked
         ind(k:n-1) = ind(k+1:n); pr(:,k:n-1) = pr(:,k+1:n); nc = nc-1; n = n-1;
      else % no, just interim
         % check children of interim element
         dim = det.sd(ind(k)); pos = det.sp(ind(k)); % split dimension & position
         if (any(d == dim)) % split along one of the condit'g planes?
            % replace interim element by child cut by condit'g plane
            if (pr(d == dim,k) <= pos) % first child
               ind(k) = det.tree(ind(k),2);
               pr(d == dim,k) = pr(d == dim,k)/pos;
            else % second child
               ind(k) = det.tree(ind(k),3);
               pr(d == dim,k) = (pr(d == dim,k)-pos)/(1-pos);
            end
         else % split does not interfere with condit'g -> proceed with both children
            n = n+1; ind(n) = det.tree(ind(k),3); pr(:,n) = pr(:,k); % add de's second child
            ind(k) = det.tree(ind(k),2); % replace interim de by de's first child
         end
         k = k+1;
      end
   end
end
leafs = leafs(1:m);
end

