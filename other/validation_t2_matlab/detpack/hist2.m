% two-dimensional adaptive histogram based on distribution element tree (det)
% mode: > 0 equal-size splits (default), < 0 equal-score splits
% zero: fraction of largest probability density considered to be zero when plotting (optional)
% bounds: [lb ub], where lb, ub are lower, upper bound column vectors of sample space (optional)
% h: histogram value
% bc, bw: bin centers and widths
% examples:
%   uniform
% n = 1E6; x = [rand(2,n/2) ([5; 0]*ones(1,n/2) + [0.2 0; 0 4]*rand(2,n/2))];
% figure, hist2(x,1,0,[-2.2 6.2; -1.4 6.4]);
%   joint Gaussian
% x = mvnrnd([0.7 5],[4.0 -2.28; -2.28 1.44],1E5)';
% figure, hist2(x,1,1E-5);
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, February 2017
function [h,bc,bw] = hist2(x, mode, zero, bounds)
% check format of input data x
if (~ismatrix(x) || (size(x,1) ~= 2))
   error(struct('identifier','hist2:inputError','message',...
      'Input data does not have 2 x n format (n is number of samples).'))
end
% check optional arguments
if (exist('mode','var')), mode = 2*(mode > 0)-1; else, mode = 1; end
if (~exist('zero','var')), zero = 0; end
% build det
if (exist('bounds','var'))
   if (any((x(1,:) < bounds(1,1)) | (bounds(1,2) < x(1,:)) | ...
           (x(2,:) < bounds(2,1)) | (bounds(2,2) < x(2,:))))
      error(struct('identifier','hist2:inputError','message','Input data outside of given bounds.'))
   end
   det = detconstruct(x, mode, bounds(:,1), bounds(:,2));
else
   det = detconstruct(x, mode, 0, 0);
end
% extract histogram bin data
[leafs,~,m] = detleafs(det);
bc = leafs.lb + leafs.s/2; bw = leafs.s; h = leafs.p;
% plot histogram
color = [205 224 247]/255;
hmax = max(h);
for k = 1:m
   if (h(k) < zero*hmax), continue, end
   vert = 0.5*[-1 -1 -1; 1 -1 -1; -1 1 -1; 1 1 -1; -1 -1 1; 1 -1 1; -1 1 1; 1 1 1];
   vert = repmat([bc(:,k); h(k)/2]',size(vert,1),1) + vert*diag([bw(:,k); h(k)]);
   face = [1 5 7 3; 2 6 8 4; 1 5 6 2; 3 7 8 4; 1 3 4 2; 5 6 8 7];
   patch('Faces',face,'Vertices',vert,'EdgeColor','k','FaceColor',color);
end
view(-40,30), xlim([det.lb(1) det.ub(1)]), ylim([det.lb(2) det.ub(2)]), grid on, hold off
end
