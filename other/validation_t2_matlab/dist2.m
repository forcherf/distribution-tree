% two-dimensional piecewise linear pdf estimator based on distribution element tree (det)
% mode: > 0 equal-size splits (default), < 0 equal-score splits
% zero: fraction of largest probability density considered to be zero when plotting (optional)
% bounds: [lb ub], where lb, ub are lower, upper bound column vectors of sample space (optional)
% h: bin values at bin corners
% bc, bw: bin centers and widths
% examples:
%   joint Gaussian
% x = mvnrnd([0.7 5],[4.0 -2.28; -2.28 1.44],1E5)';
% figure, dist2(x,1,1E-5);
%   constant x quadratic
% x = 6*rand(2,1E4); x = x(1,x(2,:) <= x(1,:)); x(1,:) = sqrt(x(1,:)); x(2,:) = rand(1,size(x,2));
% figure, dist2(x);
%   linear with peak
% x = rand(2,1E6); x = x(1,x(2,:) <= x(1,:)); x(2,:) = rand(1,size(x,2)); x = [x rand(2,1E3)/50+0.25];
% figure, dist2(x);
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, February 2017
function [h,bc,bw] = dist2(x, mode, zero, bounds)
% check format of input data x
if (~ismatrix(x) || (size(x,1) ~= 2))
   error(struct('identifier','dist2:inputError','message',...
      'Input data does not have 2 x n format (n is number of samples).'))
end
% check optional arguments
if (exist('mode','var')), mode = 4*(mode > 0)-2; else, mode = 2; end
if (~exist('zero','var')), zero = 0; end
% build det
if (exist('bounds','var'))
   if (any((x(1,:) < bounds(1,1)) | (bounds(1,2) < x(1,:)) | ...
           (x(2,:) < bounds(2,1)) | (bounds(2,2) < x(2,:))))
      error(struct('identifier','dist2:inputError','message','Input data outside of given bounds.'))
   end
   det = detconstruct(x, mode, bounds(:,1), bounds(:,2));
else
   det = detconstruct(x, mode, 0, 0);
end
% extract histogram bin data
[leafs,~,m] = detleafs(det);
bc = leafs.lb + leafs.s/2; bw = leafs.s;
h = zeros(m,2,2);
for k = 1:m
   h(k,:,:) = leafs.p(k);
   if (h(k,1,1) > 0)
      h(k,:,:) = h(k,:,:) .* reshape((([-1;1]*leafs.theta{k}(1)/2 + 1) * ...
                                      ([-1 1]*leafs.theta{k}(2)/2 + 1)), [1 2 2]);
   end
end
% plot histogram
color = [205 224 247]/255;
hmax = max(h);
for k = 1:m
   if (mean(mean(squeeze(h(k,:,:)))) < zero*hmax), continue, end
   lb = bc(:,k) - bw(:,k)/2; ub = bc(:,k) + bw(:,k)/2; % bounds of bin
   vert = [lb(1) lb(2) 0; ub(1) lb(2) 0; lb(1) ub(2) 0; ub(1) ub(2) 0; ...
           lb(1) lb(2) h(k,1,1); ...
           ub(1) lb(2) h(k,2,1); ...
           lb(1) ub(2) h(k,1,2); ...
           ub(1) ub(2) h(k,2,2)];
   face = [1 5 7 3; 2 6 8 4; 1 5 6 2; 3 7 8 4; 1 3 4 2; 5 6 8 7];
   patch('Faces',face,'Vertices',vert,'EdgeColor','k','FaceColor',color);
end
view(-40,30), xlim([det.lb(1) det.ub(1)]), ylim([det.lb(2) det.ub(2)]), grid on, hold off
end
