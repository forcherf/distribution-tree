import numpy as np
import matplotlib.pyplot as plt

import subprocess

ndata = np.array([2**i for i in (5,7,9,11,13,15,17,20)])
dim = np.array([i for i in (1,2,3,5,10,100)])

fig, ax = plt.subplots()
plt.grid(True, color='w', linestyle='-', linewidth=1,axis='y')
plt.gca().patch.set_facecolor('0.9')
plt.title("FLOPS count\n", loc='left', weight='bold')
ax.spines["left"].set_visible(False)
ax.spines["right"].set_visible(False)
ax.spines["top"].set_visible(False)


ax.set_xscale('log')
ax.set_yscale('log')
#ax.set_aspect('equal')
flops = []
for i in range(len(dim)):
    for j in range(len(ndata)):
        batcmd="sde64 -iform -mix -dyn_mask_profile -start_ssc_mark \
            FACE:repeat -stop_ssc_mark DEAD:repeat -- ./multidim_gaussian {0} {1} >/dev/null; \
            python ../../intel_sde_flops.py| grep tot_flops | cut -d: -f2".format(ndata[j],dim[i])
        print(batcmd)
        result = int(subprocess.check_output(batcmd, shell=True))
        print(result)
        flops.append(result)
    print(flops)
    ax.plot(ndata, np.array(flops), linestyle='--', marker='o', label="d=%d"%dim[i])
    print("approx beta for {1} = {0}".format( (ndata[len(ndata)-1]-ndata[len(ndata)-2]) /
                                              (ndata[len(ndata)-1]-ndata[len(ndata)-2]), dim[i]  ))
    flops=[]


ax.plot(ndata, [1000*i for i in ndata], label="O(n)")
ax.plot(ndata, [1000*i**2 for i in ndata], label="O(n^2)")
ax.plot(ndata, [1000*i*np.log(i) for i in ndata], label="O(nlogn)")

print("approx beta = ", )

ax.set_ylabel(r"Flops", rotation='horizontal')
ax.yaxis.set_label_coords(0.075,1.01)
plt.xlabel(r'N# of data')
ax.legend()

plt.show()
fig.savefig("plot1_flops.pdf", bbox_inches='tight')
