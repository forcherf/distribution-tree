#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from sys import argv

filename = argv[1] if len(argv) > 1 else 'estimate.txt'

data = np.loadtxt(filename)

plt.plot(data[:, 0], data[:, 1], color = 'red')

x = np.linspace(np.min(data[:, 0]), np.max(data[:, 0]), 200)
y = stats.norm.pdf(x)

plt.plot(x, y, color='blue')

generated = np.loadtxt('data.txt')
bins = int(np.sqrt(len(generated)))
plt.hist(generated, bins, normed = 1)

plt.show()
