// following Matlab code line by line

// data= matrix d*n

#include "typedefs.hpp"
#include <vector>
#include <tuple>

std::tuple<int, std::vector<Real>> dimstosplit(Real* x, sze, mode);
std::tuple<std::vector<int>,     // de1
           std::vector<Real>,    // sze1
           std::vector<Matrix>,  // x1
           std::vector<int>,     // de2
           std::vector<Real>,    // sze2
           std::vector<Matrix>,  // x2
           std::vector<int>      // pos
           > split(int,                // tosplit{k}(1)
      Matrix,             // x{k}
      std::vector<Real>,  // sze{k}
      int,                // ne+2k-1
      int,                // mode
);
// could be a vector of int

template <class Matrix, class Real>
std::tuple<std::vector<int>/*to change*/, int, int> detconstruct(Matrix data, int d, int n, Real lb,
                                                         Real ub, int mode = 2) {
  // line 16-38 (simplified,assuming lb, ub given)
  // nothing to do if lb, ub given

  // line 39: normalize

  for (int i = 0; i < d; i++) {
    Real length = ub[i] - lb[i];

    for (int j = 0; j < d; j++) {
      data[d * i + j] = (data[d * i + j] - lb[i]) /
                        length;  //(optimization trick: scalar replacmentent with 1/length
    }
  }

  // LINE 40-     initialize det fields (de(k) is given by tree(k,:), sd(k), sp(k), p(k), & theta{k}) :

  // % tree(k,:) contains indices of [parent child1 child2] of de(k)

  std::vector<int> tree(n * 3, 0);

  // split dimension and position of de leading to children

  std::vector<int> sd(n, 0);

  std::vector<int> sp(n, -1); // TODO maybe real 

  std::vector<Real> p(n, -1);

  // de paramenters   //TO CANCELLL
  // double* theta = new data[n];  // maybe change the type
  std::vector<std::vector<Real>> theta(n);
  // I wait the next lines of code to inizialize this

  // MAYBE TO CHANGE! THESE PROBABLY ARE LIST OF VECTOR
  // initialize lists with interim de data based on root de
  std::vector<int> dei(1, 1);  // list of interim elements

  std::vector<std::vector<Real>> sze;  // size of root de (normalized)
  std::vector<Real> sze_tmp(d, 1);
  sze.push_back(sze_tmp);

  std::vector<Matrix> x;  // samples belonging to interim elements
  x.push_back(data);

  // NOT SURE MAYBE SZE(1) AND X(1);
  auto tmp(dimstosplit(x[1], sze[1], mode));  // init. for root de

  // TODO check better the type
  std::vector<std::vector<Real>> thetai;  // list of pdf parameters of interim elements
  thetai.push_back(tmp.first);
  
  std::vector<std::vector<int>> tosplit;
  tosplit.push_back(tmp.second); // dimension(s) of interim elements to split (= 0 for no split)
  // BE CAREFULL MAYBE TO SPLIT  IS A VECTOR OF VECTOR OF INT

  // line 53:  construct de tree level-by-level

  int m = 0;    // number of tree leafs or final elements
  int ne = 1;   // number of interim and final elements
  int nt = 0;   // number of tree levels or tree depth
  Real vol = 0;  // volume of final elements

  Real voltmp;

  while (dei.size() != 0) {  // loop over the tree

    // line 60  : indices of final elements in interim de lists
    // DOUBT ABOUT TRANSPOSE OR NOT (DEPENDE OF TYPE OF TO SPLIT)
    std::vector<int> ind;
    // for (auto ptr = tosplit.begin(), int i = 0; ptr != tosplit.end(); ptr++, i++) {
    //   if (*ptr == 0)
    //     ind.push_back(i);
    // }

    int i=0;
    for (auto v : tosplit) {
      if (v[0] == 0) {
        ind.push_back(i);
      }
      i++;
    }


    // line 62
    for (int k : ind) {
      voltmp = 1;

      for (int i : sze[k]) {
        voltmp *= i;
      }

      vol += voltmp;  // Check
    }

    // 64-67    keep track of probability densities of final elements

    if (tree.size() / 3 < ne + 2 * (dei.size() - ind.size())) {
      std::vector<int> tree_reserve(3 * n, 0);
      tree.insert(tree.end(), tree_reserve.begin(), tree_reserve.end());

      std::vector<int> sd_reserve(n, 0);
      sd.insert(sd.end(),sd_reserve.begin(),sd_reserve.end();
				
			std::vector<int> sp_reserve(n,-1);
			sp.insert(sp.end(),sp_reserve.begin(),sp_reserve.end());
			
			std::vector<Real> p_reserve(n,0);
			p.insert(p.end(),p_reserve.begin(),p_reserve.end());

      std::vector<std::vector<Real> > theta_reserve(n, std::vector<Real>{});
      theta.insert(theta.end(),theta_reserve.begin(),theta_reserve.end());
      // TODO CHECK THETA INIZIALIZATION HERE IS CORRECT
    }

    // LINE 68  set probability densities of final elements

    for (int k : ind) {
      voltmp = 1;  // calculation of prod sze(k)
      for (int i : sze[k]) {
        voltmp *= i;
      }

      p[dei[k]] = (x[k].nrCols() / n)  / voltmp;  // probability density of element
      theta[dei[k]] = thetai[k];
      m++;
    }

    // discard final elements from list of interim elements
    // CHECK THIS LINES
    std::vector<int> ind2;  // indices of interim elements
    // for (auto ptr = tosplit.begin(), int i = 0; ptr != tosplit.end(); ptr++, i++) {
    //   if (*ptr[1] == 0)
    //     ind.push_back(i);
    // }
    i=0;
    for (auto v : tosplit) {
      if (v[0] > 0) {
        ind2.push_back(i);
      }
      i++;
    }



    // line 76
    std::vector<std::vector<int>> dei_tmp;
    std::vector<std::vector<Real>> sze_tmp;
    std::vector<Matrix> x_tmp;
    std::vector<int> tosplit_tmp;
    std::vector<std::vector<Real>> thetai_tmp;
    for (int k : ind) {
      dei_tmp.push_back(dei[k]);
      sze_tmp.push_back(sze[k]);
      x_tmp.push_back(x[k]);
      tosplit_tmp.push_back(tosplit[k]);
      thetai_tmp.push_back(thetai[k]);
    }
    dei = std::move(dei_tmp);
    sze = std::move(sze_temp);
    x = std::move(x_tmp);
    tosplit = std::move(tosplit_tmp);
    thetai = std::move(thetai_tmp);

    // if nt>0 erasereport
    std::clog << 100 * vol << "% completed " << m << " elements, tree depth " << nt << std::endl;

    if (dei.empty()) {
      continue;
    }
    nt++;  // add new tree level

    // line 84
    std::vector<int> dei1(dei.size(), 0);
    std::vector<std::vector<Real>> thetai1(dei.size());
    std::vector<std::vector<int>> tosplit1(dei.size());
    std::vector<std::vector<Real>> sze1(dei.size());
    std::vector<Matrix> x1(dei.size());

    std::vector<int> dei2(dei.size(), 0);
    std::vector<std::vector<Real>> thetai2(dei.size());
    std::vector<std::vector<int>> tosplit2(dei.size());
    std::vector<std::vector<Real>> sze2(dei.size());
    std::vector<Matrix> x2(dei.size());

    std::vector<int> pos(dei.size(), -1);

    // line 88 checked until here TODO
    for (int k = 0; k < dei.size(); k++) {
      // split
      auto tmpsplit = split(tosplit[k][0],  // tosplit{k}(1)
                            x[k],           // x{k}
                            sze[k],         // sze{k}
                            ne + 2k - 1,  // ne+2k-1 TODO -1 is correct???? Or is in matlab the index
                            mode,         // mode
      );
      // line 91-92
      // prepare next splits
      if (tosplit.size() > 1) {
        // 2nd split dimension from previous iteration
        tosplit1[k] = tosplit[k][2];
        tosplit2[k] = tosplit[k][2];
      } else {
         [tosplit1[k],thetai1[k]] = dimstosplit(x1[k],sze1[k],mode);
         [tosplit2[k],thetai2[k]] = dimstosplit(x2[k],sze2[k],mode);
      }
    }

    //line 101
    // update tree based on new elements (children)
    for (int k = 0; k < dei.size(); k++){
      //parent
      // TODO is ind2 == ind??? Is ind a vector??
      int ind3=dei[k];
      sd[ind3] = tosplit[k][1];
      sp[ind3] = pos[k];
      tree[3*ind3+1] = dei1[k];
      tree[3*ind3+2] = dei2[k];
      //children
      tree[3*dei1[k]+0] = ind3;
      tree[3*dei2[k]+0] = ind3;
    }
    ne = ne+2*dei.size();


    // prepare interim elements data for next tree level
    dei = dei1.insert(dei1.end(),dei2.begin(),dei2.end());
    thetai = thetai1.insert(thetai1.end(),thetai2.begin(),thetai2.end());
    tosplit = tosplit1.insert(tosplit1.end(),tosplit2.begin(),tosplit2.end());
    sze = sze1.insert(sze1.end(),sze2.begin(),sze2.end());
    x = x1.insert(x1.end(),x2.begin(),x2.end());

    tree.resize(3*ne);
    tree = tree(1:ne,:);
  }
}
