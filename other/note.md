#Optimization list
- Scalar replacement of chi2: f07aa91  OPTIMIZE-element_size
- Pivot matrix of data in place: 54cf346 OPTIMIZE-pivot_in_place
- Use binary search and store bins. 54c8445 OPTIMIZE-nohistcount
- Use partial sort 89b31d1 OPTIMIZE-giovanni-no_sort' 
- better histcounts 2: histcount2 is computed from the integer bin id of each datapoint, instead of the 
  datapoint itself, avoiding the repetition of the 1D binning. 8e12a0c OPTIMIZE-histcounts2
- vectorize the independence test (manual_flop_count branch) 1dcb5d1
- (minor) vectorize mean and stddev. 1999f6d

- Opt attempt: indep test vect, failed
    - unrolling the inner loop of independence test does not help as only one port can perform division.
- Opt attempt: use Intel IPP sort instead of std sort, failed 
    - ipp does seem to worsen performance with ippsSortAscend_64f_I compared to sort.
- Opt attempt: blocking indep test, failed: 
    - Blocking indep test D1/D2 all-to-all does not seems to increase performance.
- Opt attempt: use global sort: ?
- Opt attempt: use DAAL Intel library for quantiles, failed:
    - Library has high overhead, not intended for low latency purposes.
