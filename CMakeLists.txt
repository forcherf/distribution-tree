cmake_minimum_required(VERSION 3.2)

project(DistributionTree)

set(CMAKE_CXX_STANDARD 17)


# Uncomment to force best optimization flags.
set(CMAKE_CXX_FLAGS "-mavx2" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS_RELEASE
    "-O3 -fstrict-aliasing -funroll-loops -march=native -mtune=native -flto -DNDEBUG"
    CACHE STRING "" FORCE)

# Local libraries.
include_directories(${PROJECT_SOURCE_DIR}/include)
add_subdirectory(src)

#Profiler
set(DET_PROFILER "None" CACHE STRING "Profiler type, options are: None | Counting | PAPI.")
set_property(CACHE DET_PROFILER PROPERTY STRINGS None Counting PAPI)
if (DET_PROFILER STREQUAL "None")
  add_definitions(-DDET_WITH_NULL_PROFILER)
elseif (DET_PROFILER STREQUAL "Counting")
  add_definitions(-DDET_WITH_COUNTING_PROFILER)
  #set(PROFILING_LIB "profiling")
elseif (DET_PROFILER STREQUAL "PAPI")
  add_definitions(-DDET_WITH_PAPI_PROFILER)
  set(PROFILING_LIB "papi_profiling")
endif()


# Applications
set(DIST_TREE_LIBS ${PROFILING_LIB} distribution_tree statistical_tests util)
add_subdirectory(applications)

# Testing.
add_subdirectory(libs/googletest-1.8.0)
include(cmake/testing.cmake)
add_subdirectory(test)

