import numpy as np
import matplotlib.pyplot as plt
import sys
import re

import subprocess

def main():
    ndata = np.array([2**i for i in (5,7,9,11,13,15,17,20,22)])
    dim = np.array([i for i in (1,10,50,100,256)])

    execname = sys.argv[1] # ./build/test/performance_tests/{small|uniform}_performance_test
    m = re.search('(small)|(uniform)', execname)
    plotfileadd = m.group(0)    

    fig, ax = plt.subplots()
    plt.grid(True, color='w', linestyle='-', linewidth=1,axis='y')
    plt.gca().patch.set_facecolor('0.9')
    plt.title("Performance\n", loc='left', weight='bold')
    ax.spines["left"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)


    ax.set_xscale('log')
    ax.set_yscale('log')
    #ax.set_aspect('equal')
    times = []
    for i in range(len(dim)):
        for j in range(len(ndata)):
            batcmd="{0} {1} {2} 0 | grep -P '(Cycles)|(Time)'| cut -d':' -f2".format(execname,ndata[j],dim[i])
            print(batcmd)
            # CHECK THAT CYCLES AND TIME ARE PRINTED IN RIGHT ORDER! (Expects like "4324324\n3.21" for cycles and time in seconds)
            rtime,rcycles = subprocess.check_output(batcmd, shell=True).split()
            rtime = float(rtime)
            rcycles = int(rcycles)
            print("Cycles: {0}, Time [s]: {1}".format(rcycles,rtime))
            times.append(rtime)
        print(times)
        ax.plot(ndata, np.array(times), linestyle='--', marker='o', label="d=%d"%dim[i])
        #print("approx beta for {1} = {0}".format( (ndata[len(ndata)-1]-ndata[len(ndata)-2]) /
        #                                          (ndata[len(ndata)-1]-ndata[len(ndata)-2]), dim[i]  ))
        times=[]

    ax.set_ylabel(r"Time [s]", rotation='horizontal')
    ax.yaxis.set_label_coords(0.075,1.01)
    plt.xlabel(r'N# of data')
    ax.legend()

    #plt.show()
    fig.savefig(f'plot3_perf_{plotfileadd}.pdf', bbox_inches='tight')


if __name__== "__main__":
  main()

