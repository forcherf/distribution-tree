% extract probab. densities p from distribution element tree (det) at points x
% det: distribution element tree struct resulting from detconstruct.m
% x: d x n matrix with n query points of dimensionality d
%   1d example:
% det = detconstruct(normrnd(2,3,1,1E6),-2);
% x = [-10:0.01:10]; p = detquery(det, x); plot(x,p,'-k',x,normpdf(x,2,3),'-r')
%   2d example:
% mu = [3 5]; C = [4.0 -2.28; -2.28 1.44]; 
% f = @(x1,x2) mvnpdf([x1' x2'],mu,C)';
% lb = [-5 0]; ub = [11 10]; np = [1000 500];
% x1 = lb(1) + (ub(1)-lb(1))*([1:np(1)]-0.5)/np(1);
% x1 = reshape(x1'*ones(1,np(2)),[1 prod(np)]);
% x2 = lb(2) + (ub(2)-lb(2))*([1:np(2)]-0.5)/np(2);
% x2 = reshape(ones(np(1),1)*x2,[1 prod(np)]);
% xp = [x1; x2];
% yr = f(x1,x2); yr = reshape(yr, np);
% figure('name','reference pdf'), imagesc(x1,x2,yr), set(gca,'YDir','normal'), colorbar
% d = @(n) mvnrnd(mu,C,n)';
% yd = detquery(detconstruct(d(1E3)), xp); yd = reshape(yd, np);
% % yd = detquery(detconstruct(d(1E3),2,0,0), xp); yd = reshape(yd, np);
% figure('name','det pdf estimate'), imagesc(x1,x2,yd), set(gca,'YDir','normal'), colorbar
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, January 2017
function p = detquery(det, x)
n = size(x,2); % number of points
x = (det.A*(x-det.mu*ones(1,n)) - det.lb*ones(1,n)) ./ ((det.ub - det.lb)*ones(1,n)); % x in [0,1]
p = zeros(n,1);
% loop over points in x
%for k = 1:n
parfor k = 1:n
   xk = x(:,k);
   % check if point xk is outside of bounds -> p = 0
   if (any(xk < 0) || any(xk > 1)), p(k) = 0; continue, end
   % inside det bounds -> find de
   ind = 1; % start from root de
   while (det.tree(ind,2) > 0) % loop until leaf or final de is found
      dim = det.sd(ind); pos = det.sp(ind); % split dimension & position
      % xk in first or second child de?
      if (xk(dim) <= pos) % first child
         ind = det.tree(ind,2); % de index
         xk(dim) = xk(dim) / pos; % renormalize component
      else % second child
         ind = det.tree(ind,3); % de index
         xk(dim) = (xk(dim)-pos) / (1-pos); % renormalize component
      end
   end
   % determine probability density
   p(k) = det.p(ind);
   if (p(k) > 0), p(k) = p(k) * prod(((xk-1/2).*det.theta{ind}+1)); end
end
% rescale
p = p / prod(det.ub - det.lb);
end
