% construct distribution element tree (det) based on data d x n
% mode: +/-1 constant elements, +/-2 linear elements, > 0 equal-size splits, < 0 equal-score splits
% lb, ub: column vectors of length d with lower and upper sample space bounds (optional)
%   if lb, ub are given or are given as 0, no principal axes transform will be conducted
% det: distribution element tree struct
% m: number of leaf elements in det
% nt: maximum tree depth
% example:
% x = [zeros(1,1000) ones(1,1000) 2*rand(1,10000); zeros(1,500) ones(1,2500) 2*rand(1,9000)];
% n = 1E5; x = normrnd(0.0,1.0,1,n); x = [x; x+normrnd(0.0,0.2,1,n)];
% [det,m,nt] = detconstruct(x,1);
% treeplot(det.tree(:,1)')
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, January 2017
function [det,m,nt] = detconstruct(data, mode, lb, ub)
if (~exist('mode','var')), mode = 2; end % linear de with equal-size splits
d = size(data,1); % number of dimensions
n = size(data,2); % number of samples
% probability space or root cuboid bounds and principal axis transform
if (~exist('lb','var')) % initialize principal axes transform
   [A,~] = eigs(cov(data'),d); A = A'; % eigenvec. sorted in decr. eigenval. order
   mu = mean(data,2);
   data = A*(data-mu*ones(1,n)); % transform to principal axes
   lb = 0; ub = 0; % trigger initialization of bounds
else % lb exists -> no principal axes transform
   A = eye(d); mu = zeros(d,1);
end
if (all(lb == 0) && all(ub == 0)) % initialize bounds if not given
   lb = min(data,[],2); ub = max(data,[],2);
   for k = 1:d % treat singular case with lb = ub
      if (lb(k) == ub(k)), ub(k) = lb(k) + 10*eps(lb(k)); end
   end
else % check if data is within bounds given
   if (~(all(all(lb*ones(1,n) <= data)) && all(all(data <= ub*ones(1,n)))))
      error(struct('identifier','detconstruct:inputError','message',...
         'Input data is outside of bounds provided (lb <= data <= ub).'))
   end
end
data = (data-lb*ones(1,n)) ./ ((ub-lb)*ones(1,n)); % normalize
% initialize det fields (de(k) is given by tree(k,:), sd(k), sp(k), p(k), & theta{k})
tree = zeros(n,3); % tree(k,:) contains indices of [parent child1 child2] of de(k)
sd = zeros(n,1); sp = -ones(n,1); % split dimension and position of de leading to children
p = -ones(n,1); % de probability densities
theta = cell(n,1); % de parameters
% initialize lists with interim de data based on root de
dei = [1]; % list of interim elements
sze = {ones(d,1)}; % size of root de (normalized)
x = {data}; % samples belonging to interim elements 
thetai = {[]}; % list of pdf parameters of interim elements
tosplit = {[]}; % dimension(s) of interim elements to split (= 0 for no split)
[tosplit{1},thetai{1}] = dimstosplit(x{1},sze{1},mode); % init. for root de

% construct de tree level-by-level
m = 0; % number of tree leafs or final elements
ne = 1; % number of interim and final elements
nt = 0; % number of tree levels or tree depth
vol = 0; % volume of final elements
while (~isempty(dei)) % loop over tree levels

   % interim elements with tosplit = 0 become final elements
   ind = find(cellfun(@(x) x(1),tosplit) == 0)'; % indices of final elements in interim de lists
   for k = ind, vol = vol + prod(sze{k}); end
   % keep track of probability densities of final elements
   if (size(tree,1) < ne+2*(length(dei)-length(ind))) % allocate memory as needed
      tree = [tree; zeros(n,3)]; sd = [sd; zeros(n,1)]; sp = [sp; -ones(n,1)];
      p = [p; zeros(n,1)]; theta = [theta; cell(n,1)];
   end
   for k = ind % set probability densities of final elements
      p(dei(k)) = size(x{k},2)/n * 1/prod(sze{k}); % probability density of element
      theta{dei(k)} = thetai{k}; % pdf parameters of element
      m = m+1; % counter of final elements
   end
   % discard final elements from list of interim elements
   ind = find(cellfun(@(x) x(1),tosplit) > 0)'; % indices of interim elements
   %for k = ind, p(dei(k)) = size(x{k},2)/n * 1/prod(sze{k}); theta{dei(k)} = thetai{k}; end % pdf & params of interim elements
   dei = dei(ind); sze = sze(ind); x = x(ind);
   tosplit = tosplit(ind); thetai = thetai(ind);

   % split interim elements
   if (nt > 0), erasereport, end
   fprintf('%6.2f%% completed, %6i elements, tree depth %6i', 100*vol, m, nt)
   if (isempty(dei)), continue, end % no interim elements left -> done
   nt = nt + 1; % add new tree level
   % lists with temporary interim de data of children
   dei1 = zeros(length(dei),1); thetai1 = cell(length(dei),1); tosplit1 = thetai1; sze1 = thetai1; x1 = thetai1;
   dei2 = dei1; thetai2 = thetai1; tosplit2 = tosplit1; sze2 = sze1; x2 = x1;
   pos = -ones(length(dei),1);
   %for k = 1:length(dei) % split interim elements
   parfor k = 1:length(dei) % split interim elements
      % split
      [dei1(k),sze1{k},x1{k},dei2(k),sze2{k},x2{k},pos(k)] = ...
         split(tosplit{k}(1),x{k},sze{k},ne+2*k-1,mode);
      % prepare next splits
      if (length(tosplit{k}) > 1) % 2nd split dimension from previous iteration
         tosplit1{k} = tosplit{k}(2); tosplit2{k} = tosplit{k}(2);
      else % no 2nd split dimension -> de hypothesis testing
         [tosplit1{k},thetai1{k}] = dimstosplit(x1{k},sze1{k},mode);
         [tosplit2{k},thetai2{k}] = dimstosplit(x2{k},sze2{k},mode);
      end
   end
   % update tree based on new elements (children)
   for k = 1:length(dei)
      % parent
      ind = dei(k);
      sd(ind) = tosplit{k}(1);
      sp(ind) = pos(k);
      tree(ind,2:3) = [dei1(k) dei2(k)];
      % children
      tree(dei1(k),1) = ind; tree(dei2(k),1) = ind;
   end
   ne = ne+2*length(dei); % number of interim and final elements
   
   % prepare interim elements data for next tree level
   dei = [dei1; dei2]; thetai = [thetai1; thetai2];
   tosplit = [tosplit1; tosplit2]; sze = [sze1 sze2]; x = [x1; x2];
end % loop over tree levels

erasereport
% discard unnecessary memory
tree = tree(1:ne,:); sd = sd(1:ne); sp = sp(1:ne); p = p(1:ne); theta = theta(1:ne);
% assemble output
det = struct('tree',tree,'sd',sd,'sp',sp,'p',p,'theta',{theta},'A',A,'mu',mu,'lb',lb,'ub',ub);
end



% determine dimension(s) to split de (0 for no split)
function [dim,theta] = dimstosplit(x,sze,mode)
% is there any data?
if (isempty(x)), dim = 0; theta = []; return, end
% proceed with data
d = size(x,1); % number of dimensions
theta = zeros(d,1); p1 = zeros(d,2); h = zeros(d,1);
p1(:,2) = 1./sze; % keep track of de side lengths
for k = 1:d % loop over dimensions
   % don't test based on just one unique sample, accept hypothesized distribution instead
   if (allequal(x(k,:))), continue, end
   % test distribution
   switch abs(mode)
      case 1 % hypothesis is uniform marginal distribution
         [h(k), p1(k,1)] = chi2testuniform(x(k,:));
         %[h(k), p(k,1)] = kstestuniform(x(k,:));
         theta(k) = 0;
      case 2 % hypothesis is linear marginal distribution
         [h(k), p1(k,1), theta(k)] = chi2testlinear(x(k,:));
         %[h(k), p(k,1), theta(k)] = kstestlinear(x(k,:));
   end
end
dim = 0;
% was any marginal pdf test positive (null hypothesis rejected)?
if (any(h)) % yes, determine biggest deviating dimension (smallest p-value) and return
   % in case of equal p-value, sort by 1/size (equilibrate side lengths)
   [~, m] = sortrows(p1, [1 2]);
   % select rejecting dimension
   for k = 1:d, if (h(m(k)) == 1), dim = m(k); break, end, end
elseif ((d > 1) && (~allequal(x))) % no, check independence hypothesis
   [h, p2] = chi2indeptest(x);
   %[h, p] = tauindeptest(x);
   pl = zeros((d^2 - d)/2,2); % list of p-values
   dl = zeros(size(pl,1),2); % list of dependent dimensions
   nc = 0; % counter of dependent dimension pairs
   for j = 1:d-1
      for k = 1+j:d
         if (h(j,k) == 1) % independence hypothesis is rejected
            nc = nc+1;
            pl(nc,1) = p2(j,k); % p-value
            pl(nc,2) = 1/(sze(j)*sze(k)); % de side lengths
            dl(nc,:) = [j; k]; % dimensions
         end
      end
   end
   % check if data is independent
   if (nc > 0)
      % most dependent dimension at the top (smallest p-value)
      % in case of equal p-value, sort by 1/area (equilibrate side lengths)
      [~, dim] = sortrows(pl(1:nc,:), [1 2]);
      dim = dl(dim(1),:);
      % smaller p-value first
      if (p1(dim(1),1) >= p1(dim(2),1)), dim = fliplr(dim); end
   end
end
end



% split de or cuboid along dimension dim as given by mode (based on size or score)
function [de1,sze1,x1,de2,sze2,x2,pos] = split(dim,x,sze,id,mode)
% determine position of subdivision
if ((size(x,2) == 0) || (mode > 0)) % empty or equal size split
   pos = 0.5;
else % equal scores split
   pos = median(x(dim,:));
   % avoid elements with size zero
   if ((pos == 0) || (pos == 1)), pos = 0.5; end
end
% first subcuboid
de1 = id;
sze1 = sze; sze1(dim) = sze1(dim)*pos;
% second subcuboid
de2 = id+1;
sze2 = sze; sze2(dim) = sze2(dim)*(1-pos);
% samples in subcuboids
if (isempty(x)) % no data
   x1 = []; x2 = [];
else % split data
   ind = x(dim,:) <= pos; % samples in first subcuboid
   x1 = x(:,ind);
   if (~isempty(x1)), x1(dim,:) = x1(dim,:)/pos; end % rescale dim.
   x2 = x(:,~ind);
   if (~isempty(x2)), x2(dim,:) = (x2(dim,:)-pos)/(1-pos); end % rescale dim.
end
end



% erase previously written progress report
function erasereport
   fprintf('%s', repmat(sprintf('\b'),1,53))
end



% composite Pearson's chi^2 test for uniformly distributed random vector x in [0,1] with significance level alpha
% h is 0 or 1 if the uniform hypothesis is accepted or rejected, respectively
% p is confidence level of acceptance
% example:
% n = 100000; lb = 4; ub = 4.01;
% x = normrnd((lb+ub)/2,0.1*(ub-lb),n,1); x(x<lb | x>ub) = (lb+ub)/2; % not uniform
% x = lb + (ub-lb)*rand(n,1); % uniform
% [h,p] = chi2testuniform((x-lb)/(ub-lb))
function [h,p] = chi2testuniform(x, alpha)
if (~exist('alpha','var')), alpha = 1.0E-3; end
% cdf of uniform distribution in [0,1]
cdf = @(x) x;
n = length(x); % number of samples
% binning
[co,be] = chi2testtable(x, alpha); % observed count and bin edges
k = length(co); % number of bins
if (k <= 1), h = 0; p = 0; return, end % no testing with just one bin
ce = zeros(k,1); % expected count
for j = 1:k
   ce(j) = cdf(be(j+1));
end
ce(2:end) = ce(2:end) - ce(1:end-1); ce = n * ce;
% perform composite Pearson's chi^2 test
%plot(co), hold on, plot(ce), hold off
%[h,p,st] = chi2gof(bc,'Ctrs',bc,'Frequency',co,'Expected',ce,'EMin',0);
chi2 = sum((co-ce).^2./ce); % test statistic
p = chi2cdf(chi2, k-1, 'upper'); % = 1.0 - chi2cdf(chi2, k-1); % p-value
h = (p <= alpha); % accept or reject (0 or 1)
end



% composite Pearson's chi^2 test for linearly distributed random vector x in [0,1] with significance level alpha
% h is 0 or 1 if the linear distribution hypothesis is accepted or rejected, respectively
% p is confidence level of acceptance
% s is estimated slope
% example:
% n = 100000; lb = 4; ub = 4.01;
% x = normrnd((lb+ub)/2,0.1*(ub-lb),n,1); x(x<lb | x>ub) = (lb+ub)/2; % not linear
% x = rand(n,1); y = rand(n,1); x = x(x/2+1/2 >= y); x = lb + (ub-lb)*x; % linear
% x = lb + (ub-lb)*rand(n,1); % linear or more specifically uniform
% [h,p,s] = chi2testlinear((x-lb)/(ub-lb))
function [h,p,s] = chi2testlinear(x, alpha)
if (~exist('alpha','var')), alpha = 1.0E-3; end
% cdf of linear distribution
cdf = @(x,slp) (2+slp*(x-1))*x / 2;
n = length(x); % number of samples
% estimate mean to determine slope parameter of linear distribution
m = mean(x);
if (m == 1/2) % in case of var(x) = 0
   s = 0;
else
   s = 6*(2*m-1);
   % correction factor cf to minimize mean square error
   cf = n*s^2 / (n*s^2 + 4*36*var(x));
   s = cf*s;
end
s = min(2,max(-2,s)); % limit s to make sure pdf >= 0
% binning
[co,be] = chi2testtable(x, alpha); % observed count and bin edges
k = length(co); % number of bins
if (k <= 1), h = 0; p = 0; s = 0; return, end % no testing with just one bin
ce = zeros(k,1); % expected count
for j = 1:k
   ce(j) = cdf(be(j+1),s);
end
ce(2:end) = ce(2:end) - ce(1:end-1); ce = n * ce;
% perform composite Pearson's chi^2 test
%plot(co), hold on, plot(ce), hold off
%[h,p,st] = chi2gof(bc,'Ctrs',bc,'Frequency',co,'Expected',ce,'NParams',1,'EMin',0);
chi2 = sum((co-ce).^2./ce); % test statistic
p = chi2cdf(chi2, k-2, 'upper'); % = 1.0 - chi2cdf(chi2, k-2); % p-value
h = (p <= alpha); % accept or reject (0 or 1)
end



% test mutual uncorrelation of x components with significance level alpha
% using Pearson's chi^2 independence test
% h is 0 or 1 if independence hypothesis is accepted or rejected, respectively
% p is confidence level of acceptance
% example:
% n = 100; x = rand(3,n); x(2,:) = (x(1,:)+x(2,:))/2;
% [h,p] = chi2indeptest(x)
function [h,p] = chi2indeptest(x, alpha)
if (~exist('alpha','var')), alpha = 1.0E-3; end
% initialize things
n = size(x,1); % number of datasets in x
p = zeros(n,n); % p-value matrix
h = ones(n,n); % matrix with test results
% test for mutual independence
for i = 1:n-1
   for j = i+1:n % upper triangle
      % marginal observed counts & bin edges 
      [co1,be1] = chi2testtable(x(i,:), alpha, 1);
      [co2,be2] = chi2testtable(x(j,:), alpha, 1);
      n1 = length(co1); n2 = length(co2);
      % no need to test if there is just one bin
      if ((n1 == 1) || (n2 == 1))
         h(i,j) = 0; h(j,i) = 0; % accept
         continue
      end
      % contingency table with observed counts
      co = histcounts2(x(i,:),x(j,:),be1,be2);
      % expected counts
      ce = co1*co2' / size(x,2);
      % chi^2 independence test
      %imagesc(co), figure, imagesc(ce)
      chi2 = sum(sum((co-ce).^2./ce)); % test statistic
      p(i,j) = chi2cdf(chi2, (n1-1)*(n2-1), 'upper'); % = 1.0 - chi2cdf(chi2, (n1-1)*(n2-1)); % p-value
      p(j,i) = p(i,j);
      h(i,j) = (p(i,j) <= alpha); h(j,i) = h(i,j); % accept or reject (0 or 1)
   end
end
end



% build (contingency) table for Pearson's chi^2 tests based on 
% data x in [0,1] & significance level alpha
% c sample count in bins with edges be
% if cf is present and one, the number of bins for a contingency table is applied
% example:
% [co,be] = chi2testtable([ones(1,100), 2], 0.01)
% [co,be] = chi2testtable(rand(1,100), 0.01)
function [co,be] = chi2testtable(x, alpha, cf)
if (~exist('cf','var')), cf = 0; end
% first try without eliminating duplicates (unique is an expensive operation)
xu = x;
for t = 1:2 % attempts with and (if really needed) without duplicates
   if (t == 2), xu = unique(xu); end % second attempt, now without duplicates
   % number of bins based on rule of thumb [Cochran, Ann. of Math. Stat., 23(3), section 7]
   % and expression in [Mann & Wald, Ann. of Math. Stat., 13(3)], ...
   n = length(xu); % number of (unique) samples
   c = sqrt(2)*erfcinv(2*alpha);
   ks = n/5;
   kp = 4*(2*(n-1)^2/c^2)^(1/5);
   % ... for contingency tables see [Bagnato, Punzo, & Nicolis, J. Time Ser. Anal., 33]
   if (cf), ks = sqrt(ks); kp = sqrt(kp); end
   k = max(1, floor(min(ks, kp))); % number of bins
   % put data into bins
   xu = sort(xu);
   be = zeros(k+1,1); be(end) = 1; % bin edges
   for j = 1:k % loop over bins
      ns = round(j/k*n); % index of last sample in current bin
      if (j < k) % all but last bins
         be(j+1) = (xu(ns+1) + xu(ns)) / 2;
      end
   end
   % check if there are size-zero bins
   if (any(be(2:end) == be(1:end-1))), continue, end
   % count samples in bins
   co = histcounts(x,be)'; % observed count (incl. duplicates)
   % check if there are empty bins
   if (any(co == 0)), continue, end
end
end


% check if all column vectors in matrix x are equal
function r = allequal(x)
r = 1; n = size(x,2);
if (n > 1) % more than one vector
   for j = 2:n % loop over all vectors
      if (any(x(:,1) ~= x(:,j))), r = 0; break, end
   end
end
end
