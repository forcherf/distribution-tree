% extract characteristics of element of distribution element tree (det)
% det: distribution element tree struct resulting from detconstruct.m
% ind: index of distribution element in det
% p: probability density
% theta: de parameters
% lb: lower bound
% s: size
% div: divisions or splits along dimensions
%
% Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, January 2017
function [p,theta,lb,s,div] = de(det, ind)
d = size(det.lb,1); % number of dimensions
% initialize de property fields
lb = zeros(d,1); % lower bound
s = ones(d,1); % size
div = zeros(d,1); % subdivisions or splits
% extract de data
p = det.p(ind);
theta = det.theta{ind};
% determine de center, size, and subdivisions
while (det.tree(ind,1) > 0) % move from de to root de
   indp = det.tree(ind,1); % move one level down to parent
   dim = det.sd(indp); pos = det.sp(indp); % split dimension & position
   div(dim) = div(dim)+1;
   if (det.tree(indp,2) == ind) % first child
      s(dim) = s(dim)*pos;
      lb(dim) = lb(dim)*pos;
   else % second child
      s(dim) = s(dim)*(1-pos);
      lb(dim) = lb(dim)*(1-pos) + pos;
   end
   ind = indp;
end
% rescale
s = s .* (det.ub - det.lb);
lb = det.lb + lb .* (det.ub - det.lb);
p = p/prod(det.ub - det.lb);
end

