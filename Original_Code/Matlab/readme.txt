Density Estimation and Random Number Generation with Distribution Element Trees

Distribution element trees (DETs) enable the estimation of probability densities based on (possibly large) datasets. Moreover, DETs can be used for random number generation or smooth bootstrapping both in unconditional and conditional modes. In the latter mode, information about certain probability-space components is taken into account when sampling the remaining probability-space components. For details see http://dx.doi.org/10.1007/s11222-017-9751-9 or http://arxiv.org/abs/1610.00345 and http://dx.doi.org/10.1080/10618600.2018.1482768 or http://arxiv.org/abs/1711.04632

hist1.m and hist2.m: DET estimator for 1d and 2d data, respectively, with constant DEs
dist1.m and dist2.m: DET estimator for 1d and 2d data, respectively, with linear DEs
detconstruct.m/detquery.m: DET estimator for n-dimensional data and arbitrary query points
detrnd.m: (un)conditional random number generation based on a precomputed DET
detleafs.m and de.m: helper functions for histx.m and distx.m

Daniel Meyer, Institute of Fluid Dynamics, ETH Zurich, 2018
