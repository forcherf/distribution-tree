#pragma once

#include <array>
#include <vector>
#include <iostream>

template <class T, long unsigned int n>
std::ostream& operator<<(std::ostream& stream, const std::array<T, n>& val) {
  for (int i = 0; i < n; ++i)
    stream << val[i] << " ";
  return stream;
}

template <class T, long unsigned int n>
std::ifstream& operator>>(std::ifstream& stream, std::array<T, n>& val) {
  for (int i = 0; i < n; ++i)
    stream >> val[i];
  return stream;
}

template <class T>
std::ostream& operator<<(std::ostream& stream, const std::vector<T>& val) {
  for (int i = 0; i < val.size(); ++i)
    stream << val[i] << " ";
  return stream;
}

template <class T>
std::ifstream& operator>>(std::ifstream& stream, std::vector<T>& val) {
  for (int i = 0; i < val.size(); ++i)
    stream >> val[i];
  return stream;
}
