#pragma once

#include "matrix.hpp"
#include "typedefs.hpp"

// Generate n data distributed as d dimensional gaussian with random covariance.
Matrix<Real> generateData(int n, int d, std::size_t seed = 42);

// Generate n data with d dimensions uniformly.
Matrix<Real> generateUniform(int n, int d, std::size_t seed = 42);
