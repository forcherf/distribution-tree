#pragma once

#include "typedefs.hpp"
#include "util/counters.hpp"

#include <cmath>
#include <tuple>
#include <immintrin.h>

/* compute inverse error functions with maximum error of 2.35793 ulp */
inline Real erfinv(Real a) {
  Real p, r, t;
  t = FMA(a, 0.0 - a, 1.0);
  t = std::log(t);
  if (fabsf(t) > 6.125) {           // maximum ulp error = 2.35793
    p = 3.03697567e-10;             //  0x1.4deb44p-32
    p = FMA(p, t, 2.93243101e-8);   //  0x1.f7c9aep-26
    p = FMA(p, t, 1.22150334e-6);   //  0x1.47e512p-20
    p = FMA(p, t, 2.84108955e-5);   //  0x1.dca7dep-16
    p = FMA(p, t, 3.93552968e-4);   //  0x1.9cab92p-12
    p = FMA(p, t, 3.02698812e-3);   //  0x1.8cc0dep-9
    p = FMA(p, t, 4.83185798e-3);   //  0x1.3ca920p-8
    p = FMA(p, t, -2.64646143e-1);  // -0x1.0eff66p-2
    p = FMA(p, t, 8.40016484e-1);   //  0x1.ae16a4p-1
  }
  else {                            // maximum ulp error = 2.35456
    p = 5.43877832e-9;              //  0x1.75c000p-28
    p = FMA(p, t, 1.43286059e-7);   //  0x1.33b458p-23
    p = FMA(p, t, 1.22775396e-6);   //  0x1.49929cp-20
    p = FMA(p, t, 1.12962631e-7);   //  0x1.e52bbap-24
    p = FMA(p, t, -5.61531961e-5);  // -0x1.d70c12p-15
    p = FMA(p, t, -1.47697705e-4);  // -0x1.35be9ap-13
    p = FMA(p, t, 2.31468701e-3);   //  0x1.2f6402p-9
    p = FMA(p, t, 1.15392562e-2);   //  0x1.7a1e4cp-7
    p = FMA(p, t, -2.32015476e-1);  // -0x1.db2aeep-3
    p = FMA(p, t, 8.86226892e-1);   //  0x1.c5bf88p-1
  }
  r = a * p;
  return r;
}

inline Real erfcinv(Real a) {
  return 1. - erfinv(a);
}

inline auto meanStdv(const Real* x, int n) {
  auto mu_v = _mm256_set1_pd(0.);
  auto mu_vb = _mm256_set1_pd(0.);
  auto mu2_v = _mm256_set1_pd(0.);
  auto mu2_vb = _mm256_set1_pd(0.);
  double mu(0), mu2(0);

  int i = 0;
  while (((intptr_t)(x + i) & 0x1f) != 0) {  // alignment to 32-bit
    mu += x[i];
    mu2 += x[i] * x[i];
    ++i;
  }
  for (; i < n - 7; i += 8) {
    const auto x_v = _mm256_load_pd(x + i);
    const auto x_vb = _mm256_load_pd(x + i + 4);
    mu_v = _mm256_add_pd(mu_v, x_v);
    mu_vb = _mm256_add_pd(mu_vb, x_vb);
    mu2_v = _mm256_add_pd(mu2_v, _mm256_mul_pd(x_v, x_v));
    mu2_vb = _mm256_add_pd(mu2_vb, _mm256_mul_pd(x_vb, x_vb));
  }
  //  for (; i < n - 3; i += 4) {
  //    const auto x_v = _mm256_load_pd(x + i);
  //    mu_v = _mm256_add_pd(mu_v, x_v);
  //    mu2_v = _mm256_add_pd(mu2_v, _mm256_mul_pd(x_v, x_v));
  //  }
  mu_v = _mm256_add_pd(mu_v, mu_vb);
  mu2_v = _mm256_add_pd(mu2_v, mu2_vb);

  for (; i < n; ++i) {
    mu += x[i];
    mu2 += x[i] * x[i];
  }

  mu += (mu_v[0] + mu_v[1]) + (mu_v[2] + mu_v[3]);
  mu2 += (mu2_v[0] + mu2_v[1]) + (mu2_v[2] + mu2_v[3]);

  mu /= n;
  mu2 /= n;  // TODO: use unbiased estimator?
  const Real var = std::sqrt(mu2 - mu * mu);

  counters::flop += 3 * n;

  return std::make_tuple(mu, var);
}
