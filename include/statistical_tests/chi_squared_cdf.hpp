#pragma once

#include "typedefs.hpp"

Real chiSquaredCdf(Real x, int dof);

inline Real pValue(Real x, int dof) {
  return 1.- chiSquaredCdf(x, dof);
}
