#pragma once

#include <cstdint>
#include <tuple>

#include "matrix.hpp"
#include "typedefs.hpp"

// In: x. Matrix of real data with `dim` rows and `n` columns
// Returns: res: boolean matrix, true when the two dimensions are independent.
//          pval: pvalue relative to the independence hypothesis.
// TODO: consider if returning the acceptance is a necessity.
using Bool = std::uint8_t;  // I do not want to deal with the bitwise vector<bool>.
std::tuple<Matrix<Bool>, Matrix<Real>> chi2IndepTest(const Matrix<Real>& x, Real alpha);

namespace fast {

// Return square difference between observed count and independence ansatz.
std::tuple<Real, int, int>
chi2IndepTest(const MatrixView<Real> &x, int n_bins, const Vector<int> &co, Vector<int> &co2d,
              const Matrix<Real> &be);

}  // namespace fast
