#pragma once

#include "typedefs.hpp"

namespace fast {

// In/Out: x vector of data to sort partially.
// In: n size of x.
// In: k vector of positions in the vector x that will be sorted after the call.
// In: nk size of vector x.
void findStatistics(Real* x, int n, const int* k, int nk);

}
