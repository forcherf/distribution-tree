#pragma once

#include <array>
#include <tuple>
#include "aligned_allocator.hpp"
#include "typedefs.hpp"

using Bool = std::uint8_t;

// In: x. vector of length n.
// Returns: accept. pval.
std::tuple<Bool, Real> chiSquaredUniformTest(const Real* x, int n, Real alpha);

// In: x. vector of length n.
// Returns: accept. pval. {start, slope}.
std::tuple<Bool, Real, std::array<Real, 2>> chiSquaredLinearTest(const Real* x, int n, Real alpha);

// TODO: (maybe) add Kolmogorov–Smirnov test.

namespace fast {

//// In: x. vector of length n.
//// Returns: accept. pval.
// std::tuple<Bool, Real> chiSquaredUniformTest(const Real* x, int n, Real alpha);

// Workspaces
std::tuple<Real, Real> chiSquaredLinearTest(const Real* x, const int n, const int n_bins, int* co,
                                            Real* be, Vector<Real>& x_s, Vector<int>& ks);

}  // namespace fast
