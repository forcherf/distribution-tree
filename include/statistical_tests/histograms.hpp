#pragma once

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>
#include <matrix.hpp>

#include "statistical_tests/util.hpp"

std::tuple<std::vector<int>, std::vector<Real>> chi2testtable(const Real* x, const int n, Real alpha);

std::vector<int> histcounts(const Real* x, const std::vector<Real>& be, int n);

std::vector<int> histcounts2(const Real* __restrict__ x1, const Real* __restrict__ x2,
                             const Vector& be1, const Vector& be2, int n);

namespace fast {

// In: x, bin_id vectors of length n.
// In: be vector of length k + 1.
// Out: bin_id vector of length n.
void findBins(const Real* __restrict__ x, const Real* __restrict__ be, int k, int n,
              int* __restrict__ bin_id);

// find the number of bins for n data.
// [Bagnato, Punzo, & Nicolis, J. Time Ser. Anal., 33]
int nbins(int n, Real alpha);

// Out: co. histcounts
// Out: be. bin ends
// Workplace: x_sorted
void chi2testtable(const Real* x, const int n, const int n_bins, Real* be, Vector<Real>& x_sorted,
                   Vector<int>& ks);

// Out: co2d
// In: ld: leading dimension of co2d
void histcounts2(const int* __restrict__ x1, const int* __restrict__ x2, int k, int n, int* co2d,
                 int ld);

}  // namespace fast
