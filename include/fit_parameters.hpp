#pragma once

#include <array>
#include <vector>

#include "mode.hpp"

namespace details {
template <FitMode mode>
struct FitParameterHelper;

template <>
struct FitParameterHelper<UNIFORM> {
  using type = Real;
};

template <>
struct FitParameterHelper<LINEAR> {
  using type = std::vector<std::array<Real, 2>>;
};

}  // namespace details

template <FitMode mode>
using FitParameter = typename details::FitParameterHelper<mode>::type;
