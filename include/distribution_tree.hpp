#pragma once

#include "aligned_allocator.hpp"
#include "tree_element.hpp"
#include "matrix.hpp"

namespace fast {
template <FitMode fit_mode = UNIFORM, SplitMode split_mode = MEDIAN>
class DistributionTree;
}

template <FitMode fit_mode = UNIFORM, SplitMode split_mode = MEDIAN>
class DistributionTree {
public:
  DistributionTree() = default;

  DistributionTree(Matrix<Real>& data, Real alpha_fit = 0.01, Real alpha_indep = 0.01) {
    construct(data, alpha_fit, alpha_indep);
  }

  void construct(Matrix<Real>& data, Real alpha_fit = 0.01, Real alpha_indep = 0.01);

  Real query(std::vector<Real>& x) const {
    return query(x.data());
  }
  std::vector<Real> query(Matrix<Real>& x) const {
    assert(lb_.size() == x.nrCols());
    std::vector<Real> res(x.nrRows());
    for (int i = 0; i < x.nrRows(); ++i)
      res[i] = query(x.row(i));
    return res;
  }

  const std::vector<Real>& lowerBound() const {
    return lb_;
  }
  const std::vector<Real>& upperBound() const {
    return ub_;
  }

  std::size_t size() const {
    return elements_.size();
  }

  void write(const std::string& filaname) const;
  void read(const std::string& filename);

  bool compare(const DistributionTree<fit_mode, split_mode>& other, Real tolerance = 5e-7) const;

  friend class fast::DistributionTree<fit_mode, split_mode>;

private:
  Real query(Real* x) const;

  // Maps data to hypercube in [0, 1] and returns the original volume.
  Real normalize(Matrix<Real>& x);

  using Element = TreeElement<fit_mode>;
  std::vector<Element> elements_;
  std::vector<Real> lb_;  // lower bounds.
  std::vector<Real> ub_;  // upper bounds.
};

namespace fast {
template <FitMode fit_mode, SplitMode split_mode>
class DistributionTree {
public:
  DistributionTree() = default;

  DistributionTree(Matrix<Real>& data, Real alpha_fit = 0.01, Real alpha_indep = 0.01) {
    construct(data, alpha_fit, alpha_indep);
  }

  void construct(Matrix<Real>& data, Real alpha_fit = 0.01, Real alpha_indep = 0.01);

  Real query(std::vector<Real>& x) const {
    return query(x.data());
  }
  std::vector<Real> query(Matrix<Real>& x) const {
    assert(lb_.size() == x.nrCols());
    std::vector<Real> res(x.nrRows());
    for (int i = 0; i < x.nrRows(); ++i)
      res[i] = query(x.row(i));
    return res;
  }

  const auto& lowerBound() const {
    return lb_;
  }
  const auto& upperBound() const {
    return ub_;
  }

  std::size_t size() const {
    return elements_.size();
  }

  //  void write(const std::string &filaname) const;
  //  void read(const std::string &filename);

  bool compare(const ::DistributionTree<fit_mode, split_mode>& other, Real tolerance = 5e-7) const;

private:
  // Maps data to hypercube in [0, 1] and returns the original volume.
  Real normalize(Matrix<Real>& x);

  Real query(Real* x) const;

  using Element = fast::TreeElement;
  Vector<Element> elements_;
  Vector<Real> lb_;  // lower bounds.
  Vector<Real> ub_;  // upper bounds.
  Matrix<Real> data_;

  // workspaces
  Vector<Real> slope_;
  Vector<Real> sums_;
  Vector<int> ks_;
  Matrix<Real> be_;
  Vector<int> co_;
  Vector<int> co2d_;
  Vector<Real> x_sorted_;
};

}  // namespace fast
