#pragma once

#include <vector>
#include <stdlib.h>

namespace fast {

template <typename T>
class AlignedAllocator  {
public:
  using value_type = T;
  using pointer = T*;
  using const_pointer = const T*;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;

T* allocate(std::size_t n) {
    if (!n)
      return nullptr;

    T* ptr;
    int err = posix_memalign((void**)&ptr, 64, n * sizeof(T));
    if (err)
      throw(std::bad_alloc());
    return ptr;
  }

  void deallocate(T*& ptr, std::size_t /*n*/ = 0) noexcept {
    free(ptr);
    ptr = nullptr;
  }
};

// return that all specializations of this allocator are interchangeable
template <class T1, class T2>
bool operator== (const AlignedAllocator<T1>&,
                 const AlignedAllocator<T2>&) throw() {
  return true;
}
template <class T1, class T2>
bool operator!= (const AlignedAllocator<T1>&,
                 const AlignedAllocator<T2>&) throw() {
  return false;
}


template<class T>
using Vector = std::vector<T, AlignedAllocator<T>>;

} // namespace fast
