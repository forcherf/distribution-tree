// Profiler selector
#pragma once

#if defined(DET_WITH_COUNTING_PROFILER)
#include "counting_profiler.hpp"
#include "events/time_event.hpp"
using Profiler = profiling::CountingProfiler<profiling::time_event<std::size_t>>;

#elif defined(DET_WITH_PAPI_PROFILER)
#include "counting_profiler.hpp"
#include "events/papi_and_time_event.hpp"
using Profiler = profiling::CountingProfiler<profiling::PapiAndTimeEvent>;

#else // Use NullProfiler.
#include "null_profiler.hpp"
using Profiler = profiling::NullProfiler;

#endif
