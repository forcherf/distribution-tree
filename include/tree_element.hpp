#pragma once

#include <array>
#include <vector>
#include "matrix.hpp"
#include "mode.hpp"
#include "typedefs.hpp"
#include "fit_parameters.hpp"

// Tree TreeElement.
template <FitMode mode>
struct TreeElement {
  TreeElement() : fit_parameter(0) {}

  Matrix<Real> x;  // data: matrix d*n.
  std::vector<Real> sze;
  int to_split = -1;
  int child1 = -1;
  int child2 = -1;
  FitParameter<mode> fit_parameter;
  Real split_point = 0;
};

namespace fast {

struct TreeElement {
  TreeElement() = default;

  MatrixView<Real> x;  // data: matrix d*n.
  int to_split = -1;
  int child1 = -1;
  int child2 = -1;
  Vector<Real> slope;
  Real density = 0; // reused for storing the volume on non-leaves.
  Real split_point = 0;
};

}  // namespace fast
