#pragma once

#include <cassert>
#include <vector>

#include "aligned_allocator.hpp"

template <class T>
class MatrixView;

template <class T>
class Matrix {
public:
  Matrix() : Matrix(0, 0){};
  Matrix(Matrix<T>&& rhs) = default;
  Matrix(const Matrix<T>& rhs) = default;

  Matrix<T>& operator=(Matrix<T>&& rhs) {
    n_rows_ = rhs.n_rows_;
    n_cols_ = rhs.n_cols_;
    data_ = std::move(rhs.data_);
    return *this;
  }

  Matrix<T>& operator=(const Matrix<T>& rhs) = default;

  Matrix(int nrows, int ncols) : n_rows_(nrows), n_cols_(ncols), data_(ncols * nrows) {}
  Matrix(int nrows, int ncols, T val) : n_rows_(nrows), n_cols_(ncols), data_(ncols * nrows, val) {}

  void resize(int n, int m) {
    n_rows_ = n;
    n_cols_ = m;
    data_.resize(n * m);
  }

  void clear() {
    n_rows_ = n_cols_ = 0;
    fast::Vector<T>().swap(data_);
  }

  T& operator()(int i, int j) {
    assert(i < n_rows_ && j < n_cols_);
    return data_[i * n_cols_ + j];
  }

  const T& operator()(int i, int j) const {
    assert(i < n_rows_ && j < n_cols_);
    return data_[i * n_cols_ + j];
  }

  int nrCols() const {
    return n_cols_;
  }
  int nrRows() const {
    return n_rows_;
  }
  int ld() const {
    return n_cols_;
  }

  const T* row(int i) const {
    assert(i < n_rows_);
    return data_.data() + n_cols_ * i;
  }

  T* row(int i) {
    assert(i < n_rows_);
    return data_.data() + n_cols_ * i;
  }

  operator MatrixView<T>() {
    return MatrixView<T>(*this);
  }

private:
  int ldm_;
  int n_rows_;
  int n_cols_;
  fast::Vector<T> data_;
};

extern "C" {
void dgemm_(const char* transa, const char* transb, const int* n, const int* m, const int* k,
            const double* alpha, const double* a, const int* lda, const double* b, const int* ldb,
            const double* beta, double* c, const int* ldc);
}

inline Matrix<double> operator*(const Matrix<double>& a, const Matrix<double>& b) {
  const int m = a.nrRows();
  const int n = b.nrCols();
  const int k = a.nrCols();
  assert(k == b.nrRows());
  const char op = 'N';

  Matrix<double> c(m, n);
  const double alpha = 1.;
  const double beta = 0.;

  const int lda = a.nrCols();
  const int ldb = b.nrCols();
  const int ldc = c.nrCols();
  dgemm_(&op, &op, &n, &m, &k, &alpha, &b(0, 0), &ldb, &a(0, 0), &lda, &beta, &c(0, 0), &ldc);
  return c;
}

inline Matrix<double> square(const Matrix<double>& a) {
  const int n = a.nrRows();
  Matrix<double> c(n, n);
  const char op1 = 'N';
  const char op2 = 'T';
  const int lda = a.nrCols();
  const double alpha = 1.;
  const double beta = 0.;
  dgemm_(&op1, &op2, &n, &n, &n, &alpha, &a(0, 0), &lda, &a(0, 0), &lda, &beta, &c(0, 0), &lda);
  return c;
}

// Lightweight copy of a row major matrix, no allocation is performed.
template <class T>
class MatrixView {
public:
  MatrixView() : n_rows_(0), n_cols_(0), ldm_(0), ptr_(nullptr) {}

  MatrixView(Matrix<T>& m, int n_cols, int offset_col)
      : n_rows_(m.nrRows()), n_cols_(n_cols), ldm_(m.nrCols()), ptr_(&m(0, offset_col)) {}

  MatrixView(MatrixView<T>& m, int n_cols, int offset_col)
      : n_rows_(m.nrRows()), n_cols_(n_cols), ldm_(m.ldm_), ptr_(&m(0, offset_col)) {}

  MatrixView(const MatrixView<T>& rhs) = default;
  MatrixView& operator=(const MatrixView<T>& rhs) = default;

  T& operator()(int i, int j) {
    assert(i <= nrRows() && j <= nrCols());
    return ptr_[i * ldm_ + j];
  }

  const T& operator()(int i, int j) const {
    assert(i <= nrRows() && j <= nrCols());
    return ptr_[i * ldm_ + j];
  }

  int nrCols() const {
    return n_cols_;
  }
  int nrRows() const {
    return n_rows_;
  }

  const T* row(int i) const {
    return ptr_ + ldm_ * i;
  }
  T* row(int i) {
    return ptr_ + ldm_ * i;
  }

private:
  int n_rows_;
  int n_cols_;
  int ldm_;
  T* ptr_;
};
