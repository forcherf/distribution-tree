#pragma once

#include <array>
#include <tuple>

#include "matrix.hpp"
#include "typedefs.hpp"
#include "mode.hpp"
#include "fit_parameters.hpp"

template <FitMode mode>
std::tuple<std::array<int, 2>, FitParameter<mode>> dimstosplit(const Matrix<Real>& x,
                                                               const std::vector<Real>& sze,
                                                               Real alpha_fit, Real alpha_indep);

// Returns: id1, size1, x1, id2, size2, x2, split_point
std::tuple<Real, Matrix<Real>, Real, Matrix<Real>, Real> split(int dim, const Matrix<Real>& x,
                                                               Real size, SplitMode mode);

namespace fast {

template <FitMode mode>
std::array<int, 2>
dimstosplit(const MatrixView<Real> &x, Real alpha_fit, Real alpha_indep, Vector<Real> &slope,
            Vector<Real> &sums, Vector<int> &ks, Vector<int> &co2d,
            Vector<Real> &x_sorted, Matrix<Real> &be, Vector<int> &co);

// Returns: id1, size1, x1, id2, size2, x2, split_point
std::tuple<MatrixView<Real>, MatrixView<Real>, Real> split(int dim, MatrixView<Real>& x, SplitMode mode);
}  // namespace fast
