#pragma once

#include <vector>

// Note: comment this line out for single precision.
#define DOUBLE_PRECISION

#ifdef DOUBLE_PRECISION
using Real = double;
#define FMA fma
#else  // DOUBLE_PRECISION
using Real = double;
#define FMA fmaf
#endif   // DOUBLE_PRECISION

using Vector = std::vector<Real>;

// For flop count, see other/test_flopcount
#ifndef __SSC_MARK
#define __SSC_MARK(tag)                                                        \
        __asm__ __volatile__("movl %0, %%ebx; .byte 0x64, 0x67, 0x90 "         \
                             ::"i"(tag) : "%ebx")
#endif
